<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role=new Role();
        $role->name='admin';
        $role->display_name='administrador';
        $role->description='puede hacer de todo';
        $role->status=1;

        $role->save();
        $role->permissions()->attach(1);
        $role->permissions()->attach(2);
        $role->permissions()->attach(3);


    }
}
