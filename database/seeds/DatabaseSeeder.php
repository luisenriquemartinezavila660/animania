<?php

use App\Models\CategoryCharacter;
use App\Models\Character;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(AnimatorSeeder::class);
        $this->call(CategoryCharacterSeeder::class);
        $this->call(CategoryGarmentSeeder::class);
        $this->call(CategoryPropSeeder::class);
        $this->call(CharacterSeeder::class);
        $this->call(DiscountSeeder::class);
        $this->call(GarmentSeeder::class);
        //$this->call(PackageSeeder::class);
        $this->call(PostSeeder::class);
        $this->call(PropSeeder::class);
        $this->call(WorkerSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(EventTableSeeder::class);
        //$this->call(ContractTableSeeder::class);
    }
}
