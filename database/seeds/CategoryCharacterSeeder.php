<?php

use Illuminate\Database\Seeder;
use App\Models\CategoryCharacter;

class CategoryCharacterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat=new CategoryCharacter();
        $cat->name='Animado';
        $cat->description='ergerghergergerger';
        $cat->save();

        $cat=new CategoryCharacter();
        $cat->name='Live-action';
        $cat->description='ergwefgergerger';
        $cat->save();

        $cat=new CategoryCharacter();
        $cat->name='botarga';
        $cat->description='weffdfsefwdef';
        $cat->save();
    }
}
