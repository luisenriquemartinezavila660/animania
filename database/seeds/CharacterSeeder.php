<?php

use App\Models\Character;
use Illuminate\Database\Seeder;

class CharacterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat=new Character();
        $cat->name='bob esponja';
        $cat->source='caricatura';
        $cat->photo='ruta';
        $cat->character_category_id='1';
        $cat->status='1';
        $cat->cost='200';
        $cat->save();

        $cat=new Character();
        $cat->name='power ranger rojo';
        $cat->source='los power rangers';
        $cat->photo='ruta';
        $cat->character_category_id='2';
        $cat->status='1';
        $cat->cost='200';
        $cat->save();

        $cat=new Character();
        $cat->name='blanca nieves';
        $cat->source='blanca nieves y los 7 enanitos';
        $cat->photo='ruta';
        $cat->character_category_id='1';
        $cat->status='1';
        $cat->cost='200';
        $cat->save();
    }
}
