<?php

use Illuminate\Database\Seeder;
use App\Models\Discount;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $disc=new Discount();
        $disc->name='descuento uno';
        $disc->percentage='50';
        $disc->save();

        $disc=new Discount();
        $disc->name='descuento dos';
        $disc->percentage='20';
        $disc->save();

        $disc=new Discount();
        $disc->name='descuento tres';
        $disc->percentage='40';
        $disc->save();
    }
}
