<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new Permission();
        $permission->name='Create';
        $permission->display_name='Crear';
        $permission->description='permite crear cualquier registro en las tablas admitidas';
        $permission->save();

        $permission = new Permission();
        $permission->name='Edit';
        $permission->display_name='Editar';
        $permission->description='permite editar cualquier registro en las tablas admitidas';
        $permission->save();

        $permission = new Permission();
        $permission->name='Delete';
        $permission->display_name='Eliminar';
        $permission->description='permite eliminar cualquier registro en las tablas admitidas';
        $permission->save();


    }
}
