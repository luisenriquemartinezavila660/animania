<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pos = new Post();
        $pos->name='zanquero';
        $pos->description='anda en zancos';
        $pos->cost='500';
        $pos->save();

        $pos = new Post();
        $pos->name='botarga';
        $pos->description='todo tipo de botargas';
        $pos->cost='250';
        $pos->save();

        $pos = new Post();
        $pos->name='personaje';
        $pos->description='persona disfrasada de lo que sea';
        $pos->cost='200';
        $pos->save();

        $pos = new Post();
        $pos->name='staff';
        $pos->description='de audio,cargatodo,etc';
        $pos->cost='150';
        $pos->save();
    }
}
