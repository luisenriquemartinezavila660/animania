<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name="Avila";
        $user->email="animania@admin.com";
        $user->age='20';
        $user->telephone='87565';
        $user->password = bcrypt('luna');
        $user->status=1;
        $user->role_id=1;
        $user->save();

        $user = new User();
        $user->name="Marisol";
        $user->email="Marisol Pacheco Nicolas";
        $user->age='20';
        $user->telephone='87565';
        $user->password = bcrypt('M424Lu20P43nN1M4L0v2211202019s0lFor()3v3r');
        $user->status=1;
        $user->role_id=1;
        $user->save();
    }
}
