<?php

use Illuminate\Database\Seeder;
use App\Models\Prop;

class PropSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prop = new Prop();
        $prop->name='tripie';
        $prop->category_prop_id='1';
        $prop->stock='7';
        $prop->save();

        $prop = new Prop();
        $prop->name='bocina';
        $prop->category_prop_id='2';
        $prop->stock='9';
        $prop->save();

        $prop = new Prop();
        $prop->name='microfono';
        $prop->category_prop_id='3';
        $prop->stock='16';
        $prop->save();

        $prop = new Prop();
        $prop->name='cajas';
        $prop->category_prop_id='4';
        $prop->stock='9';
        $prop->save();
    }
}
