<?php

use Illuminate\Database\Seeder;
use App\Models\Contract;

class ContractTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contrato= new Contract();
        $contrato->package_id=1;
        $contrato->name='esteban garcia';
        $contrato->telephone='98765';
        $contrato->address='colonia de la estrella';
        $contrato->name_celebrate='pepito';
        $contrato->type_event='fiesta infantil';
        $contrato->age_celebrated='4 años';
        $contrato->date_celebrated='2020-03-01';
        $contrato->date_contract='2020-03-01';
        $contrato->references='en face me entere';
        $contrato->total='5600';
        $contrato->status=1;
        $contrato->save();
    }
}
