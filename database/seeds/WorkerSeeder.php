<?php

use Illuminate\Database\Seeder;
use App\Models\Worker;

class WorkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $work=new Worker();
        $work->name='luis enrique';
        $work->age='20';
        $work->address='colonia 7 regiones';
        $work->telephone='9515831008';
        $work->email='luis@gmail.com';
        $work->status='1';
        $work->save();

        $work=new Worker();
        $work->name='Daylin belem';
        $work->age='20';
        $work->address='en su casa';
        $work->telephone='9515831008';
        $work->email='day@gmail.com';
        $work->status='1';
        $work->save();
    }
}
