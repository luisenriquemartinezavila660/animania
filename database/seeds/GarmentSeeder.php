<?php

use Illuminate\Database\Seeder;
use App\Models\Garment;


class GarmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gar= new Garment();
        $gar->name='chaleco salbabidas';

        $gar->stock='6';
        $gar->category_garment_id='1';
        $gar->save();

        $gar= new Garment();
        $gar->name='falda';

        $gar->stock='6';
        $gar->category_garment_id='1';
        $gar->save();
    }
}
