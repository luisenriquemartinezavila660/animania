<?php

use Illuminate\Database\Seeder;
use App\Models\Animator;


class AnimatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $anim = new Animator();
        $anim->name='jose';
        $anim->photo='ruta';
        $anim->experience='2 años';
        $anim->telephone='2355432';
        $anim->cost='200';
        $anim->email='drg4@gmail.com';
        $anim->save();

        $anim = new Animator();
        $anim->name='manuel';
        $anim->photo='ruta';
        $anim->experience='4 años';
        $anim->telephone='23534';
        $anim->cost='200';
        $anim->email='frthr@gmail.com';
        $anim->save();

        $anim = new Animator();
        $anim->name='daylin';
        $anim->photo='ruta';
        $anim->experience='3 años';
        $anim->cost='200';
        $anim->telephone='235443563';
        $anim->email='day@gmail.com';
        $anim->save();
    }
}
