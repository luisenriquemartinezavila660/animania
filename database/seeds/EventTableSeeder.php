<?php

use Illuminate\Database\Seeder;
use App\Models\Event;


class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event=new Event();
        $event->name='Fiesta';
        $event->description='fiesta infantil de 4 horas';
        $event->cost=1000;
        $event->save();

        $event=new Event();
        $event->name='Visita sorpresa';
        $event->description='visita de aproximadamente 45 min';
        $event->cost=500;
        $event->save();
    }
}
