<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContractsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice');
            $table->integer('package_id')->nullable(true);
            $table->integer('location_id')->nullable(true);
            $table->string('name');
            $table->string('telephone');
            $table->string('name_celebrate');
            $table->integer('event_id')->nullable(true);
            $table->string('age_celebrated');
            $table->date('date_celebrated')->nullable(true);
            $table->date('date_contract')->nullable(true);
            $table->string('hour_celebrated');
            $table->decimal('total',10,2)->nullable(true);
            $table->integer('status')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts');
    }
}
