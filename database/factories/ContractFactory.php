<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contract;
use Faker\Generator as Faker;

$factory->define(Contract::class, function (Faker $faker) {

    return [
        'package_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'address' => $faker->word,
        'name_celebrate' => $faker->word,
        'date_celebrated' => $faker->word,
        'date_contract' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
