<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Prop;
use Faker\Generator as Faker;

$factory->define(Prop::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'category_prop_id' => $faker->randomDigitNotNull,
        'stock' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
