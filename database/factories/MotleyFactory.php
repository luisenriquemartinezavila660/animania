<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Motley;
use Faker\Generator as Faker;

$factory->define(Motley::class, function (Faker $faker) {

    return [
        'character' => $faker->word,
        'source' => $faker->word,
        'stock' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'cost' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
