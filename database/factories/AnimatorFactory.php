<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Animator;
use Faker\Generator as Faker;

$factory->define(Animator::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'photo' => $faker->word,
        'experience' => $faker->word,
        'telephone' => $faker->word,
        'email' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
