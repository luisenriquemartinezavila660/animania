<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Garment;
use Faker\Generator as Faker;

$factory->define(Garment::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'type_garment' => $faker->randomDigitNotNull,
        'stock' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
