@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
        Users edit
    </h1>
</section>
<div class="content">

    <div class="box-primary">
        <div class="box-body">

                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}


                        <div class="col-md-12">
                            <!-- jquery validation -->
                            <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Usuarios <small>Nueva</small></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('users.fields')
                            </div>
                            <!-- /.card -->
                        </div>

                    {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection
