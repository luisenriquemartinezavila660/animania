@extends('layouts.app')
@section('content')
@if(\Auth::user()->can('Create'))

<div class="row justify-content-md-center">
    <div class="col-md-4">
        <div class="col align-self-center">
            <h1 class="pull-right ">
            <a class="btn btn-block btn-primary btn-lg" href="{{ url('/users/create') }}">Añadir Usuario</a>
            </h1>
        </div>

    </div>
</div>
@endif

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Todos los Usuarios</h3>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">nombre</th>
                    <th scope="col">edad</th>
                    <th scope="col">correo</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Administrar</th>
                  </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->age}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{isset($user->roles->name)?$user->roles->name:'Sin Rol Asignado'}}</td>
                    <td>
                    <td>{!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @if(\Auth::user()->can('Edit'))

                        <a href="{{ route('users.edit', [$user->id]) }}" class='btn btn-info btn-sm'><i class="fas fa-pencil-alt"></i> Editar</a>
                        @endif
                        @if(\Auth::user()->can('Delete'))

                        {!! Form::button('<i class="fas fa-trash"></i> Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Esta seguro de eliminar al usuario: $user->name ?')"]) !!}
                        @endif
                    </div>
              {!! Form::close() !!}</td>
              </tr>
          @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>


@endsection
