<div class="form-group col-sm-12">
    {!! Form::label('name', 'nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="row">
    <div class="form-group col-sm-3">
        {!! Form::label('age', 'Edad:') !!}
        {!! Form::text('age', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-9">
        {!! Form::label('email', 'Correo:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-3">
        {!! Form::label('telephone', 'Telefono:') !!}
        {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-9">
        <label for="exampleFormControlSelect1">Example select</label>
        <select class="form-control" id="role_id" name="role_id">
        @foreach($roles as $role)
        <option value="{{$role->id}}">{{$role->name}}</option>
        @endforeach
        </select>
    </div>
</div>
@if(isset($user))
@else
<div class="form-group col-sm-6 offset-md-3">
    {!! Form::label('password', 'Contraseña:') !!}
    {!! Form::text('password', null, ['class' => 'form-control']) !!}
</div>
@endif




<div class="row">
	<div class="offset-md-3 col-md-2">
		<a href="{!! route('users.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
			<i class="fas fa-reply"></i>
			<p>Atras</p>
		</a>
	</div>
	<div class="col-md-2">
		<button class="btn btn-block bg-gradient-success btn-flat " type="submit">
			<i class="	fas fa-save"></i>
			<p>Guardar</p>
		</button>
    </div>
</div>
