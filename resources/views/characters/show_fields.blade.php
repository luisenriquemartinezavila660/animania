<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{{ $character->name }}</p>
</div>

<!-- Source Field -->
<div class="form-group">
    {!! Form::label('source', 'Fuente:') !!}
    <p>{{ $character->source }}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Foto/imagen:') !!}
    <p>{{ $character->photo }}</p>
</div>


<!-- Cost Field -->
<div class="form-group">
    {!! Form::label('cost', 'Costo:') !!}
    <p>{{ $character->cost }}</p>
</div>

