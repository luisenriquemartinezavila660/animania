@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nuevo Personaje
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">

                    {!! Form::open(['route' => 'characters.store', 'enctype'=>'multipart/form-data']) !!}


                        <div class="col-md-12">
                            <!-- jquery validation -->
                            <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Personaje <small>Nuevo</small></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('characters.fields')
                            </div>
                            <!-- /.card -->
                        </div>

                    {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
