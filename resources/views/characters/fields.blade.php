<!-- Name Field -->
<div class="row">
    <div class="offset-md-1 form-group col-sm-4">
        {!! Form::label('name', 'Nombre:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Source Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('source', 'Fuente:') !!}
        {!! Form::text('source', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-3">
        {!! Form::label('cost', 'Costo:') !!}
        {!! Form::number('cost', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="offset-md-1 form-group col-sm-4">
        {!! Form::label('character_category_id', 'Categoria:') !!}
        {!! Form::select('character_category_id',$categories, null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-3">
        {!! Form::label('photo', 'Fotografia/imagen:') !!}
        @if(isset($character->photo))
            <input type="file" name="photo" class="form-control" value="{{$character->photo}}">
            @else
                {!! Form::file('photo',null, ['class' => 'form-control']) !!}
        @endif
        @if(isset($character->photo))
            <img src="{{asset('storage').'/'.$character->photo}}" width="150">
        @endif
    </div>
</div>

<!-- Photo Field -->







<!-- Submit Field -->
<div class="row">
	<div class="offset-md-3 col-md-2">
		<a href="{!! route('characters.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
			<i class="fas fa-reply"></i>
			<p>Atras</p>
		</a>
	</div>
	<div class="col-md-2">
		<button class="btn btn-block bg-gradient-success btn-flat " type="submit">
			<i class="	fas fa-save"></i>
			<p>Guardar</p>
		</button>
    </div>
</div>
