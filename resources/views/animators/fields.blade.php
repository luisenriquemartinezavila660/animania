<!-- Name Field -->
<div class="row">
    <div class="form-group col-md-6 ">
        {!! Form::label('name', 'Nombre:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
    {!! Form::label('experience', 'Experiencia:') !!}
    {!! Form::text('experience', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('telephone', 'Telefono:') !!}
        {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Correo:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="custom-file col-sm-6">
            {!! Form::label('photo', 'Photo:') !!}
            @if(isset($animator->photo))
            <input type="file" name="photo" class="form-control" value="{{$animator->photo}}">
            @else
                {!! Form::file('photo',null, ['class' => 'form-control']) !!}
        @endif
        @if(isset($animator->photo))
            <img src="{{asset('storage').'/'.$animator->photo}}" width="150">
        @endif
</div>
    <!-- Submit Field -->
    <div class="row">
        <div class="offset-md-3 col-md-2">
            <a href="{!! route('animators.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
                <i class="fas fa-reply"></i>
                <p>Atras</p>
            </a>
        </div>
        <div class="col-md-2">
            <button class="btn btn-block bg-gradient-success btn-flat " type="submit">
                <i class="  fas fa-save"></i>
                <p>Guardar</p>
            </button>
        </div>
    </div>






