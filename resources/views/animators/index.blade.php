@extends('layouts.app')

@section('content')
@if(\Auth::user()->can('Create'))
    <div class="row justify-content-md-center">
        <div class="col-md-4">
            <div class="col align-self-center">
                <h1 class="pull-right ">
                <a class="btn btn-block btn-primary btn-lg" href="{{ route('animators.create') }}">Añadir Animador</a>
                </h1>
            </div>

        </div>
    </div>
@endif
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('animators.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

