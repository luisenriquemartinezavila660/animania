@extends('layouts.app')

@section('content')

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class=" box-primary">
            <div class="box box-body">
                <div>
                    {!! Form::open(['route' => 'animators.store', 'enctype'=>'multipart/form-data']) !!}


                        <div class="col-md-12">
                            <!-- jquery validation -->
                            <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Animad@ <small>Nuev@</small></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('animators.fields')
                            </div>
                            <!-- /.card -->
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
