@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Animator
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">

                   {!! Form::model($animator, ['route' => ['animators.update', $animator->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

                   <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Animad@ <small>editar</small></h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @include('animators.fields')
                    </div>
                    <!-- /.card -->
                </div>

                   {!! Form::close() !!}

           </div>
       </div>
   </div>
@endsection
