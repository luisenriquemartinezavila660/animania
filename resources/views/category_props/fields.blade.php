<!-- Name Field -->
<div class="row">
    <div class=" offset-md-2 form-group col-md-6">
        {!! Form::label('name', 'Nombre:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Description Field -->
    <div class="offset-md-2 form-group col-md-6">
        {!! Form::label('description', 'Descripcion:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

</div>

<div class="row">
	<div class="offset-md-3 col-md-2">
		<a href="{!! route('categoryProps.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
			<i class="fas fa-reply"></i>
			<p>Atras</p>
		</a>
	</div>
	<div class="col-md-2">
		<button class="btn btn-block bg-gradient-success btn-flat " type="submit">
			<i class="	fas fa-save"></i>
			<p>Guardar</p>
		</button>
    </div>
</div>

