@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$homes['characters']}}</h3>

                <p>Personajes</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-friends"></i></i>
              </div>
            <a href="{{url('characters/')}}" class="small-box-footer">
                Mas informacion <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$homes['animators']}}</h3>

                <p>Animadores</p>
              </div>
              <div class="icon">
                <i class="far fa-smile"></i></i>
              </div>
              <a href="{{url('animators/')}}" class="small-box-footer">
                Mas informacion <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$homes['workers']}}</h3>

                <p>Trabajadores</p>
              </div>
              <div class="icon">
                <i class="fas fa-people-carry"></i></i>
              </div>
            <a href="{{url('workers/')}}" class="small-box-footer">
                Mas informacion <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$homes['contracts']}}</h3>

                <p>Contratos activos</p>
              </div>
              <div class="icon">
                <i class="fas fa-chart-pie"></i>
              </div>
            <a href="{{url('home/')}}" class="small-box-footer">
                Proximamente <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>

    </div>
</div>
@endsection
