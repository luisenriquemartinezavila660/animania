<div class="table-responsive">
    <table class="table" id="suits-table">
        <thead>
            <tr>
                <th>Character</th>
        <th>Garment Id</th>
        <th>Stock</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suits as $suit)
            <tr>
                <td>{{ $suit->character }}</td>
            <td>{{ $suit->garment_id }}</td>
            <td>{{ $suit->stock }}</td>
            <td>{{ $suit->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suits.destroy', $suit->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suits.show', [$suit->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('suits.edit', [$suit->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
