@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Suit
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suit, ['route' => ['suits.update', $suit->id], 'method' => 'patch']) !!}

                        @include('suits.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection