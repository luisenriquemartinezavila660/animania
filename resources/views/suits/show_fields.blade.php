<!-- Character Field -->
<div class="form-group">
    {!! Form::label('character', 'Character:') !!}
    <p>{{ $suit->character }}</p>
</div>

<!-- Garment Id Field -->
<div class="form-group">
    {!! Form::label('garment_id', 'Garment Id:') !!}
    <p>{{ $suit->garment_id }}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{{ $suit->stock }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $suit->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $suit->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $suit->updated_at }}</p>
</div>

