<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{{ $prop->name }}</p>
</div>

<!-- Category Prop Id Field -->
<div class="form-group">
    {!! Form::label('category_prop_id', 'Categoria:') !!}
    <p>{{ $prop->category_prop_id }}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Existencias:') !!}
    <p>{{ $prop->stock }}</p>
</div>



