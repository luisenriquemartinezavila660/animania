@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Editar Utileria
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">

                   {!! Form::model($prop, ['route' => ['props.update', $prop->id], 'method' => 'patch']) !!}

                   <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Utileria <small>Editar</small></h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @include('props.fields')
                    </div>
                    <!-- /.card -->
                </div>

                   {!! Form::close() !!}

           </div>
       </div>
   </div>
@endsection
