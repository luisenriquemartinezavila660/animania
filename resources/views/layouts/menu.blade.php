<li class="{{ Request::is('contracts*') ? 'active' : '' }}">
    <a href="{{ route('contracts.index') }}"><i class="fa fa-edit"></i><span>Contracts</span></a>
</li>

<li class="{{ Request::is('locations*') ? 'active' : '' }}">
    <a href="{{ route('locations.index') }}"><i class="fa fa-edit"></i><span>Locations</span></a>
</li>

<li class="{{ Request::is('events*') ? 'active' : '' }}">
    <a href="{{ route('events.index') }}"><i class="fa fa-edit"></i><span>Events</span></a>
</li>

