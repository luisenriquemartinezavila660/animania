@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Editar Puesto
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">

                   {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'patch']) !!}

                   <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Puesto de Trabajo <small>Nuevo</small></h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @include('posts.fields')
                    </div>
                    <!-- /.card -->
                </div>

                   {!! Form::close() !!}

           </div>
       </div>
   </div>
@endsection
