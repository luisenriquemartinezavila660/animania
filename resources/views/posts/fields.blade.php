<!-- Name Field -->
<div class="row">
    <div class="form-group col-sm-9">
        {!! Form::label('name', 'Nombre:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Description Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('cost', 'costo:') !!}
        {!! Form::number('cost', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group col-sm-12">
        {!! Form::label('description', 'Descripcion:') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>



<!-- Submit Field -->
<div class="row">
    <div class="offset-md-3 col-md-2">
        <a href="{!! route('posts.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
            <i class="fas fa-reply"></i>
            <p>Atras</p>
        </a>
    </div>
    <div class="col-md-2">
        <button class="btn btn-block bg-gradient-success btn-flat " type="submit">
            <i class="  fas fa-save"></i>
            <p>Guardar</p>
        </button>
    </div>
</div>
