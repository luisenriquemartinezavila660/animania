@extends('welcome')
@section('pages')
<div class="slider_area">
    <div class="single_slider  d-flex align-items-center slider_bg_1 overlay">
        <div class="container">
            <div class="row align-items-center justify-content-start">
                <div class="col-lg-10 col-md-10">
                    <div class="slider_text">
                        <img src="{{ asset('visit/img/animania.png')}}" width="300" height="250" class="img-fluid">
                        <h3 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".1s">
                            ANIMANIA LUNA
                        </h3>
                        <a class="boxed-btn3 wow fadeInLeft"  data-wow-duration="1s" data-wow-delay=".2s" href="https://animania-contratos.herokuapp.com/">Cotiza tu Show</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->

<!-- service_area  -->

<!--/ service_area  -->

<div class="about_area">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-5 offset-lg-1">
                <div class="about_info">
                    <div class="section_title white_text">
                        <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Sobre Nosotros</span>
                        <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">Historia </h3>
                        <p class="mid_text wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Animanía Luna es creado hace 4 años con la finalidad de brindar una nueva forma de divertir a los niños de Oaxaca ofreciendo el nuevo concepto de shows temáticos.

Animanía Luna tiene como fundadores a la familia Luna Brigido, juntos capacitan a chicos y chicas que les fascine el maravilloso mundo de los niños y así formen parte de esta gran familia ANIMANIA LUNA.</p>
                        <p class="last_text wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">En Animanía Luna todos somos familia, y tenemos un objetivo.

¡Hacer felices a los niños!</p>
                        <a href="#" class="boxed-btn3 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">Mas Sobre Nosotros</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- portfolio_image_area  -->
<!--<div class="portfolio_image_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center mb-90">
                    <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Nuestra Galeria</h3>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">...</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="single_Portfolio wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="portfolio_thumb">
                        <img src="{{ asset('visit/img/portfolio/1.png')}}" alt="">
                    </div>
                    <div class="portfolio_hover">
                        <div class="title">
                            <span>App Design</span>
                                <h3>Colorlib Mobile App</h3>
                                <a class="boxed-btn3" href="portfolio_details.html">View</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_Portfolio wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
                    <div class="portfolio_thumb">
                        <img src="{{ asset('visit/img/portfolio/2.png')}}" alt="">
                    </div>
                    <div class="portfolio_hover">
                        <div class="title">
                            <span>App Design</span>
                                <h3>Colorlib Mobile App</h3>
                                <a class="boxed-btn3" href="portfolio_details.html">View</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-lg-4">
                <div class="single_Portfolio wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <div class="portfolio_thumb">
                        <img src="{{ asset('visit/img/portfolio/3.png')}}" alt="">
                    </div>
                    <div class="portfolio_hover">
                        <div class="title">
                            <span>App Design</span>
                                <h3>Colorlib Mobile App</h3>
                                <a class="boxed-btn3" href="portfolio_details.html">View</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-lg-4">
                <div class="single_Portfolio wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
                    <div class="portfolio_thumb">
                        <img src="{{ asset('visit/img/portfolio/4.png')}}" alt="">
                    </div>
                    <div class="portfolio_hover">
                        <div class="title">
                            <span>App Design</span>
                                <h3>Colorlib Mobile App</h3>
                                <a class="boxed-btn3" href="portfolio_details.html">View</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-lg-4">
                <div class="single_Portfolio wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                    <div class="portfolio_thumb">
                        <img src="{{ asset('visit/img/portfolio/5.png')}}" alt="">
                    </div>
                    <div class="portfolio_hover">
                        <div class="title">
                            <span>App Design</span>
                                <h3>Colorlib Mobile App</h3>
                                <a class="boxed-btn3" href="portfolio_details.html">View</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!--/ portfolio_image_area  -->

<div class="how_we_work_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="work_info">
                    <div class="section_title">
                        <h3 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".3s">Siguenos en Youtube</h3>
                        <p class="mid_text wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".4s">¿Quieres ver como es nuestro trabajo?</p>
                        <p class="last_p wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">"con nosotros,tu vendicion pasara un dia magico e inolvidable"</p>
                    </div>
                    <div class="video_watch d-flex align-items-center">
                        <div class="play_btn wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
                            <a href="https://www.youtube.com/watch?v=BFHJJr3Pr8E" class="video_icon popup-video"> <i class="fa fa-play"></i> </a>
                        </div>
                       <span class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".7s"> Ver video</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- team_member_start -->
<div class="team_area ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center mb-90">
                    <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Nuestro Equipo de Animadores</h3>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">Contamos con un gran equipo de animadores talentosos y carismaticos que haran reir,bailar y brincar a todos los niños.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
            @foreach($animators as $animator)
                <div class="col-lg-3 col-md-6">
                    <div class="single_team wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">
                        <div class="team_thumb">
                            <img src="{{asset('storage').'/'.$animator->photo}}">
                            <div class="team_hover">
                                <div class="hover_inner text-center">
                                    <ul>
                                        <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                        <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                        <li><a href="#"> <i class="fa fa-instagram"></i> </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team_title text-center">
                            <h3>{{$animator->name}}</h3>
                            <p>{{$animator->email}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            <!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
        </div>
    </div>
</div>
<!--/ team_member_end -->

<!-- testimonial_area  -->
<!--<div class="testimonial_area ">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="testmonial_active owl-carousel">
                    <div class="single_carousel">
                            <div class="single_testmonial text-center">
                                    <div class="quote">
                                        <img src="{{ asset('visit/img/testmonial/quote.svg')}}" alt="">
                                    </div>
                                    <p>Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor <br>
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec sed neque.  <br>
                                            Fusce ac mattis nulla. Morbi eget ornare dui. </p>
                                    <div class="testmonial_author">
                                        <div class="thumb">
                                                <img src="{{ asset('visit/img/testmonial/thumb.png')}}" alt="">
                                        </div>
                                        <h3>Robert Thomson</h3>
                                        <span>Business Owner</span>
                                    </div>
                                </div>
                    </div>
                    <div class="single_carousel">
                            <div class="single_testmonial text-center">
                                    <div class="quote">
                                        <img src="{{ asset('visit/img/testmonial/quote.svg')}}" alt="">
                                    </div>
                                    <p>Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor <br>
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec sed neque.  <br>
                                            Fusce ac mattis nulla. Morbi eget ornare dui. </p>
                                    <div class="testmonial_author">
                                        <div class="thumb">
                                                <img src="{{ asset('visit/img/testmonial/thumb.png')}}" alt="">
                                        </div>
                                        <h3>Robert Thomson</h3>
                                        <span>Business Owner</span>
                                    </div>
                                </div>
                    </div>
                    <div class="single_carousel">
                            <div class="single_testmonial text-center">
                                    <div class="quote">
                                        <img src="{{ asset('visit/img/testmonial/quote.svg')}}" alt="">
                                    </div>
                                    <p>Donec imperdiet congue orci consequat mattis. Donec rutrum porttitor <br>
                                            sollicitudin. Pellentesque id dolor tempor sapien feugiat ultrices nec sed neque.  <br>
                                            Fusce ac mattis nulla. Morbi eget ornare dui. </p>
                                    <div class="testmonial_author">
                                        <div class="thumb">
                                                <img src="{{ asset('visit/img/testmonial/thumb.png')}}" alt="">
                                        </div>
                                        <h3>Robert Thomson</h3>
                                        <span>Business Owner</span>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!-- /testimonial_area  -->

<div data-scroll-index="0" class="get_in_tauch_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center mb-90">
                    <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Contactanos</h3>
                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">Dudas o sugerencias aqui mismo,nosotros te escucharemos y te responderemos.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="touch_form">
                    <form action="#">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                    <input type="text" placeholder="nombre completo" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
                                    <input type="email" placeholder="Correo" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                                    <input type="email" placeholder="Asunto" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single_input wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
                                   <textarea name="" id="" cols="30" placeholder="Mensaje" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="submit_btn wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                                    <button class="boxed-btn3" type="submit">ENVIAR</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
