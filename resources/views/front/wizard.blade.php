@extends('wizard')
@section('pages')
			<!-- Google Tag Manager (noscript) -->
	<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
	<!-- End Google Tag Manager (noscript) -->
	
	<div class="image-container set-full-height" style="background-image: url('images/paper-1.jpg')">
	    <!--   Creative Tim Branding   -->
	    <!-- <a href="https://creative-tim.com">
	         <div class="logo-container">
	            <div class="logo">
	                <img src="img/new_logo.png">
	            </div>
	            <div class="brand">
	                Creative Tim
	            </div>
	        </div>
	    </a> -->

		<prueba></prueba>

	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">

		            <!--      Wizard container        -->
		            <div class="wizard-container">

		                <div class="card wizard-card" data-color="green" id="wizardProfile">
		                    <form action="wizard.blade.php" method="post">
		                <!--        You can switch " data-color="orange" "  with one of the next bright colors: "blue", "green", "orange", "red", "azure"          -->

		                    	<div class="wizard-header text-center">
		                        	<h3 class="wizard-title">Formulario de Cotizacion</h3>
									<p class="category">Sigue los pasos rellenando lo que se pide para completar tu cotizacion correctamente.</p>
		                    	</div>

								<div class="wizard-navigation">
									<div class="progress-with-circle">
									     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
									</div>
									<ul>
			                            <li>
											<a href="#about" data-toggle="tab">
												<div class="icon-circle">
												<i class="ti-user"></i>
												</div>
												Datos Personales
											</a>
										</li>
			                            <li>
											<a href="#address" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-map"></i>
												</div>
												Direccion
											</a>
										</li>
			                            <li>
											<a href="#event" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-agenda"></i>
												</div>
												Evento
											</a>
										</li>
										<!-- <li>
											<a href="#package" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-package"></i>
												</div>
												Paquete
											</a>
										</li> -->
										<li>
											<a href="#animator" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-microphone"></i>
												</div>
												Animador
											</a>
										</li>
										<li>
											<a href="#caracters" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-wand"></i>
												</div>
												Personajes
											</a>
										</li>
										<li>
											<a href="#total" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-pencil-alt"></i>
												</div>
												Total
											</a>
										</li>
										<li>
											<a href="#price" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-notepad"></i>
												</div>
												Cotizacion
											</a>
										</li>
			                        </ul>
								</div>
		                        <div class="tab-content">
		                            <div class="tab-pane" id="about">
		                            	<div class="row">
											<h5 class="info-text"> Necesitamos mas detalles.</h5>
																					
											<div class="col-sm-10 col-sm-offset-1">
											<div class="form-group">
													<label>Nombre del Solicitador <small>(Necesario)</small></label>
													<input name="NSolicitador" type="text" class="form-control" placeholder="Nombre">
												</div>
												<div class="form-group">
													<label>Telefono <small>(Necesario)</small></label>
													<input name="STelefono" type="tel" class="form-control" placeholder="(951) 555 55 55">
												</div>
												<div class="form-group">
													<label>Nombre del Fetejado <small>(Necesario)</small></label>
													<input name="NFestejado" type="text" class="form-control" placeholder="Nombre">
												</div>
												<div class="form-group">
													<label>Edad Festejado </label>
													<input name="FEdad" type="number" class="form-control" placeholder="">
												</div>
												<div class="form-group col-md-7">
													<label>Fecha del Evento <small>(Necesario)</small></label>
													<input name="DEvento" type="date" class="form-control" placeholder="04/05/21">
												</div>
												<div class="form-group col-md-5">
													<label>Hora del evento <small>(Necesario)</small></label>
													<input name="DEvento" type="text" class="form-control" placeholder="09:30 o 13:20">
												</div>
											</div>
										</div>
		                            </div>
		                            <div class="tab-pane" id="address">
		                                <!-- <h5 class="info-text"> What are you doing? (checkboxes) </h5>
		                                <div class="row">
		                                    <div class="col-sm-8 col-sm-offset-2">
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Design">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-paint-roller"></i>
															<p>Design</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Code">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-pencil-alt"></i>
															<p>Code</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Develop">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-star"></i>
															<p>Develop</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div> -->
										<div class="row">
											<h5 class="info-text"> Ingresa la direccion del evento </h5>

											<div class="col-sm-10 col-sm-offset-1">
												<div class="form-group">
													<label>Direccion del evento <small>(Necesario)</small></label>
													<input name="AEvento" type="text" class="form-control" >
												</div>
												<!--Coordenadas del lugar-->

												<div class="form-group">
													<label>Referencias </label>
													<input name="EReferencias" type="textarea" class="form-control" >
												</div>
											</div>

											<div class="col-sm-10 col-sm-offset-1"/*d-none d-sm-block mb-5 pb-4">
												<div id="map" style="height: 480px;"></div>
												<script>
												var map = L.map('map').setView([17.087036, -96.694501], 16);
												L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
													attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
												}).addTo(map);
												L.marker([17.087036, -96.694501]).addTo(map)
													.bindPopup('Aqui se encuentra<br> ANIMANIA LUNA.')
													.openPopup();
												</script>
											</div>
											
										</div>
		                            </div>
		                            <div class="tab-pane" id="event">
		                                <h5 class="info-text"> Escoja un tipo de evento </h5>
		                                <div class="row">
		                                    <div class="col-sm-8 col-sm-offset-3">
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="event" value="Fiesta">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-music-alt"></i>
															<p>Fiesta</p>
		                                                </div> 
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="event" value="Sorpresa">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-gift"></i>
															<p>Visita Sorpresa</p>
		                                                </div>
		                                            </div>
		                                        </div>		                                        
		                                    </div>
		                                </div>
		                            </div>

									<div class="tab-pane" id="package">
		                                <h5 class="info-text"> Escoja un paquete </h5>
		                                <div class="row">
		                                    <div class="col-sm-8 col-sm-offset-2">
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="package" value="Paquete 1">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-shield"></i>
															<p>Paquete 1</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="package" value="Paquete 2">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-bag"></i>
															<p>Paquete 2</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="package" value="Paquete 3">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-star"></i>
															<p>Paquete 3</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>

									<div class="tab-pane" id="animator">
		                                <h5 class="info-text"> Escoja un animador </h5>
		                                <div class="row">
										@foreach($animators as $animator)
		                                    <div class="col-sm-8 col-sm-offset-2">
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Design">
		                                                <div class="card card-checkboxes card-hover-effect">
		                                                    <i class="ti-video-camera"></i>
															<p>{{$animator->name}}</p>
		                                                </div>
		                                            </div>
		                                        </div>
											</div>
										@endforeach
		                                </div>
		                            </div>

									<div class="tab-pane" id="caracters">
									<h5 class="info-text"> Escoja sus personajes </h5>
		                                <div class="row">
		                                    <div class="col-sm-14 col-sm-offset-2">
		                                        <div class="col-sm-2">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Design">
		                                                <div class="card card-checkboxes card-hover-effect">
															<img src="images/characters/cap.jpg" width="80" height="120">															
															<p>Animador 1</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-2">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Code">
		                                                <div class="card card-checkboxes card-hover-effect">
															<img src="images/characters/gamora.jpg" width="80" height="120">
															<p>Animador 2</p>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-2">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Develop">
		                                                <div class="card card-checkboxes card-hover-effect">
															<img src="images/characters/ironman.jpg" width="80" height="120">
															<p>Animador 3</p>
		                                                </div>
		                                            </div>
		                                        </div>										
												<div class="col-sm-2">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Code">
		                                                <div class="card card-checkboxes card-hover-effect">
															<img src="images/characters/spider.jpg" width="80" height="120">
															<p>Animador 4</p>
		                                                </div>
		                                            </div>
		                                        </div>
												<div class="col-sm-2">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Code">
		                                                <div class="card card-checkboxes card-hover-effect">
															<img src="images/characters/widow.jpg" width="80" height="120">
															<p>Animador 5</p>
		                                                </div>
		                                            </div>
		                                        </div>												
		                                    </div>
		                                </div>
		                            </div>
									<!-- ----------------------------------------------------------- -->
									<div class="tab-pane" id="total">
										<div class="row">
											<div class="col-sm-12">
												<h5 class="info-text"> Revise sus datos </h5>
											</div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Nombre Solicitador: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label> </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Mateo  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Telefono: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label> </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>951 000 11 22  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Nombre Festejado: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label> </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Andred  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Fecha Evento: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label> </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>06/08/21  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Direccion: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label> </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Direccion  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Evento: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label>$50 </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Fiesta  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Paquete: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label>$80 </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Paquete 1  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Animador: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label>$20 </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Animador 1  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>

											<div class="col-sm-3 col-sm-offset-1">
											<div class="form-group">
		                                            <label>Personaje: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-7">
		                                        <div class="form-group">
													<label>$10 </label>
														<div class="col-sm-7">
														<div class="form-group">
															<label>Personaje 1  </label>
														</div>
													</div>
		                                        </div>
		                                    </div>
											
											
											<div class="col-sm-3 col-sm-offset-6">
											<div class="form-group">
		                                            <label>Total: </label>																								                                            
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-3">
		                                        <div class="form-group">
													<label>$160 </label>
		                                        </div>
		                                    </div>
																																																														
										</div>
		                            </div>

									<!-- ------------------------------------------------------------- -->
									<div class="tab-pane" id="price">
		                                <div class="row">
		                                    <div class="col-sm-12">
		                                        <h5 class="info-text"> Da clic en Generar PDF </h5>
		                                    </div>
											
											<center>
												<a href="{{ url('/imprimir-pdf') }}" target="_blank" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Generar PDF</a>
											</center>
											
		                                </div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
		                            <div class="pull-right">
		                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Next' />
		                                <a type='button' href="/"class='btn btn-finish btn-fill btn-warning btn-wd' name='finish' value='Finish' > Finalizar</a>
		                            </div>

		                            <div class="pull-left">
		                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	    	</div><!-- end row -->
		</div> <!--  big container -->

	    <!-- <div class="footer">
	        <div class="container text-center">
	            // here used to be a message to go a external https://www.creative-tim.com/product/paper-bootstrap-wizard"	        
			</div>
	    </div> -->
		<!-- <div class="fixed-plugin">
			<div class="dropdown open">
				<a href="#" data-toggle="dropdown">
					<i class="fa fa-cog fa-2x"> </i>
				</a>
				<ul class="dropdown-menu">
					<li class="header-title">Examples</li>
					<li class="active">
						<a href="wizard-create-profile.html">
						   <img src="img/wizard-create-profile.png">
						   Create Profile
						</a>
					</li>
					<li>
						<a href="wizard-find-desk.html">
						   <img src="img/wizard-find-desk.png">
						   Find Desk
						</a>
					</li>
					<li>
						<a href="wizard-list-place.html">
						   <img src="img/wizard-list-place.png">
						   List Your Place
						</a>
					</li>
					<li class="header-title">Colors</li>
					<li class="adjustments-line">
						<a href="javascript:void(0)" class="switch-trigger">
							<div class="text-center">
								<span class="badge filter badge-blue" data-color="blue"></span>
								<span class="badge filter badge-azure" data-color="azure"></span>
								<span class="badge filter badge-green" data-color="green"></span>
								<span class="badge filter badge-orange active" data-color="orange"></span>
								<span class="badge filter badge-red" data-color="red"></span>
							</div>
							<div class="clearfix"></div>
						</a>
					</li>
				   <li class="tutorial">
						<div class="text-center">
							<a href="documentation/elements.html" target="_blank" class="btn btn-default btn-fill btn-block">Documentation</a>
						</div>
					</li>
					<li class="license">
						Personal License
						<div class="text-center">
							<a href="https://www.creative-tim.com/product/paper-bootstrap-wizard" target="_blank" class="btn btn-info btn-fill btn-block">Download, it's free!</a>
						</div>
					</li>
				</ul>
			</div>
		</div> -->
	</div>
@endsection