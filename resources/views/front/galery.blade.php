@extends('welcome')
@section('pages')
     <!-- bradcam_area  -->
     <div class="bradcam_area breadcam_bg_2">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3>Nuestra Galeria</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /bradcam_area  -->

    <div class="section-top-border">
        <h3>Nuestra Galeria</h3>
        <div class="row gallery-item">
            <div class="col-md-4">
                <a href="visit/img/banner/cap.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/cap.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="visit/img/banner/1.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/1.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="visit/img/banner/2.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/2.jpg);"></div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="visit/img/banner/3.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/3.jpg);"></div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="visit/img/banner/4.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/4.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="visit/img/banner/5.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/5.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="visit/img/banner/6.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/6.jpg);"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="visit/img/banner/7.jpg" class="img-pop-up">
                    <div class="single-gallery-image" style="background: url(visit/img/banner/7.jpg);"></div>
                </a>
            </div>
        </div>
    </div>
@endsection
