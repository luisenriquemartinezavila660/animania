@extends('welcome')
@section('pages')
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
<div class="bradcam_area breadcam_bg_3">
    <div class="container">
      <div class="row">
          <div class="col-xl-12">
              <div class="bradcam_text">
                  <h3>Contactanos</h3>
              </div>
          </div>
      </div>
    </div>
  </div>
  <!-- /bradcam_area  -->
  <!-- ================ contact section start ================= -->
  <section class="contact-section section_padding">
    <div class="container">
      <div class="d-none d-sm-block mb-5 pb-4">
        <div id="map" style="height: 480px;"></div>
        <script>
          var map = L.map('map').setView([17.087036, -96.694501], 16);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        L.marker([17.087036, -96.694501]).addTo(map)
            .bindPopup('Aqui se encuentra<br> ANIMANIA LUNA.')
            .openPopup();
        </script>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>Dirección</h3>
              <p>Calle Puerto Escondido #112. Col. 7 Regiones</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
            <div class="media-body">
              <h3>Telefono</h3>
              <p>(951)5090530</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3>Correo Electronico</h3>
              <p>animanialuna1@hotmail.com</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
