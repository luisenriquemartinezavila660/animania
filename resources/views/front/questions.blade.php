@extends('welcome')
@section('pages')
     <!-- bradcam_area  -->
     <div class="bradcam_area breadcam_bg_3">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3>Preguntas Frecuentes</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial_area ">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="testmonial_active owl-carousel">
                        <div class="single_carousel">
                                <div class="single_testmonial text-center">
                                        <div class="quote">
                                            <img src="img/testmonial/quote.svg" alt="">
                                        </div>
                                        <p>¿Con cuanto tiempo puedo apartar el show?</p>
                                        <div class="testmonial_author">
                                            <div class="thumb">

                                            </div>
                                            <h3>El show puede apartarse desde 4 meses antes. Entre más cercana sea su fecha, menos posibilidad existe que este disponible su personaje favorito.</h3>

                                        </div>
                                    </div>
                        </div>
                        <div class="single_carousel">
                                <div class="single_testmonial text-center">
                                        <div class="quote">
                                            <img src="img/testmonial/quote.svg" alt="">
                                        </div>
                                        <p>¿El show es totalmente infantil?</p>
                                        <div class="testmonial_author">
                                            <div class="thumb">
                                                    <img src="img/testmonial/thumb.png" alt="">
                                            </div>
                                            <h3>Sí, cuidamos cada detalle de los shows para que tu y toda tu familia disfruten del show. (humor 100% blanco)</h3>

                                        </div>
                                    </div>
                        </div>
                        <div class="single_carousel">
                                <div class="single_testmonial text-center">
                                        <div class="quote">
                                            <img src="img/testmonial/quote.svg" alt="">
                                        </div>
                                        <p>¿Que tiempo tengo para depositar, después de haber apartado la fecha por teléfono? </p>
                                        <div class="testmonial_author">
                                            <div class="thumb">
                                                    <img src="img/testmonial/thumb.png" alt="">
                                            </div>
                                            <h3>Su fecha se respalda por teléfono 72 Horas. Si después de las 72 horas no se realizo ningún contrato la fecha vuelve a estar disponible para futuros clientes.</h3>

                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
