<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{{ $garment->name }}</p>
</div>

<!-- Type Garment Field -->
<div class="form-group">
    {!! Form::label('category_garment_id', 'Categoria:') !!}
    <p>{{ $category_garment_id }}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{{ $garment->stock }}</p>
</div>

