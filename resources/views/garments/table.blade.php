

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Todas las Prendas</h3>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Existencias</th>
                    <th>categoria</th>
                    <th colspan="3">Administrar</th>
                </tr>
            </thead>
            <tbody>
            @foreach($garments as $garment)
              <tr>
                <td>{{ $garment->name }}</td>
                <td>{{ $garment->stock }}</td>
                <td>{{isset($garment->categoryGarments->name)?$garment->categoryGarments->name:'Sin Categoria'}}</td>
                <td>{!! Form::open(['route' => ['garments.destroy', $garment->id], 'method' => 'delete']) !!}
              <div class='btn-group'>

                @if(\Auth::user()->can('Edit'))

                <a href="{{ route('garments.edit', [$garment->id]) }}" class='btn btn-info btn-sm'><i class="fas fa-pencil-alt"></i> Editar</a>
                 @endif
                @if(\Auth::user()->can('Delete'))

                {!! Form::button('<i class="fas fa-trash"></i> Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Esta seguro de eliminar la prenda: $garment->name ?')"]) !!}
                @endif
            </div>
              {!! Form::close() !!}</td>
              </tr>
          @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
