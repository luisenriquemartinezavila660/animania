<div class="table-responsive">
    <table class="table" id="motleys-table">
        <thead>
            <tr>
                <th>Character</th>
        <th>Source</th>
        <th>Stock</th>
        <th>Status</th>
        <th>Cost</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($motleys as $motley)
            <tr>
                <td>{{ $motley->character }}</td>
            <td>{{ $motley->source }}</td>
            <td>{{ $motley->stock }}</td>
            <td>{{ $motley->status }}</td>
            <td>{{ $motley->cost }}</td>
                <td>
                    {!! Form::open(['route' => ['motleys.destroy', $motley->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('motleys.show', [$motley->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('motleys.edit', [$motley->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
