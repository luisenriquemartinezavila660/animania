<!-- Character Field -->
<div class="form-group col-sm-6">
    {!! Form::label('character', 'Character:') !!}
    {!! Form::text('character', null, ['class' => 'form-control']) !!}
</div>

<!-- Source Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source', 'Source:') !!}
    {!! Form::text('source', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('motleys.index') }}" class="btn btn-default">Cancel</a>
</div>
