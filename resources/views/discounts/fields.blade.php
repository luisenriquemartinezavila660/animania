<!-- Name Field -->
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nombre:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('percentage', 'Porcentaje:') !!}
        {!! Form::number('percentage', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="row">
	<div class="offset-md-3 col-md-2">
		<a href="{!! route('discounts.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
			<i class="fas fa-reply"></i>
			<p>Atras</p>
		</a>
	</div>
	<div class="col-md-2">
		<button class="btn btn-block bg-gradient-success btn-flat " type="submit">
			<i class="	fas fa-save"></i>
			<p>Guardar</p>
		</button>
    </div>
</div>
