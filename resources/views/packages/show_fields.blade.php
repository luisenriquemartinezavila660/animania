<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $package->name }}</p>
</div>

<!-- Motley Id Field -->
<div class="form-group">
    {!! Form::label('motley_id', 'Motley Id:') !!}
    <p>{{ $package->motley_id }}</p>
</div>

<!-- Worker Id Field -->
<div class="form-group">
    {!! Form::label('worker_id', 'Worker Id:') !!}
    <p>{{ $package->worker_id }}</p>
</div>

<!-- Character Id Field -->
<div class="form-group">
    {!! Form::label('character_id', 'Character Id:') !!}
    <p>{{ $package->character_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $package->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $package->updated_at }}</p>
</div>

