@extends('layouts.app')
@section('content')

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">

                    {!! Form::open(['route' => 'roles.store']) !!}


                        <div class="col-md-12">
                            <!-- jquery validation -->
                            <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Rol <small>Nuevo</small></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('roles.fields')
                            </div>
                            <!-- /.card -->
                        </div>

                    {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
