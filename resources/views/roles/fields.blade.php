<div class="form-group col-sm-12">
    {!! Form::label('name', 'nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 offset-md-3">
    {!! Form::label('display_name', 'display_name:') !!}
    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('description', 'descripcion:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!--<div class="form-group col-sm-6">
    <label for="exampleFormControlSelect1">Example select</label>
    <select class="form-control" id="permission_id" name="permission_id">
    @foreach($permissions as $permission)
    <option value="{{$permission->id}}">{{$permission->name}}</option>
    @endforeach
    </select>
</div>-->
<div class="row">
    <div class="offset-md-1 col-md-6 ">
        {!! Form::label('permissions', 'Asignacion de Permisos a Usuario:') !!}<br>
        @foreach($permissions as $permission)
        <div class="checkbox">
            <label>
                {{Form::checkbox('permission_id[]', $permission->id,(isset($role)?($role->permissions->find($permission->id)?true:false):false),['id'=>'$permission->id', 'class'=>'uk-checkbox'] )}} {{ $permission->display_name }}
            </label>
        </div>
        @endforeach
    </div>
</div>

<div class="row">
	<div class="offset-md-3 col-md-2">
		<a href="{!! route('roles.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
			<i class="fas fa-reply"></i>
			<p>Atras</p>
		</a>
	</div>
	<div class="col-md-2">
		<button class="btn btn-block bg-gradient-success btn-flat " type="submit">
			<i class="	fas fa-save"></i>
			<p>Guardar</p>
		</button>
    </div>
</div>
