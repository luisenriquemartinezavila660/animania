@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        Categoria de personajes
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">

                   {!! Form::model($categoryCharacter, ['route' => ['categoryCharacters.update', $categoryCharacter->id], 'method' => 'patch']) !!}

                   <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Categoria <small>Editar</small></h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @include('category_characters.fields')
                    </div>
                    <!-- /.card -->
                </div>

                   {!! Form::close() !!}

           </div>
       </div>
   </div>
@endsection
