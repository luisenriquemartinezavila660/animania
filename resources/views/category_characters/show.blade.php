@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        Categoria de personajes
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">

                    <div class="col-md-12">
                        <!-- jquery validation -->
                        <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Categoria <small>Ver</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('category_characters.show_fields')
                        <div class="row">
                            <div class="offset-md-3 col-md-2">
                                <a href="{!! route('categoryCharacters.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
                                    <i class="fas fa-reply"></i>
                                    <p>Atras</p>
                                </a>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
