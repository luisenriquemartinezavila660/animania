@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        Categorias de Prendas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">

                   {!! Form::model($categoryGarment, ['route' => ['categoryGarments.update', $categoryGarment->id], 'method' => 'patch']) !!}

                   <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Categoria <small>Nueva</small></h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @include('category_garments.fields')
                    </div>
                    <!-- /.card -->
                </div>

                   {!! Form::close() !!}

           </div>
       </div>
   </div>
@endsection
