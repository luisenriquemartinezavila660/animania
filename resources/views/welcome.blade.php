<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Animania Luna |Shows Infantiles y Sociales</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <!-- Place favicon.ico in the root directory -->
    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset('visit/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/slick.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{ asset('visit/css/style.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>
<body>
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                    <div class="row align-items-center no-gutters">
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo-img">
                                <a href="{{url('/')}}">
                                    <img src="{{ asset('visit/img/animania.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8">
                            <div class="main-menu  d-none d-lg-block text-center">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="active" href="{{url('/')}}">Inicio</a></li>
                                        <li><a href="{{url('/servicios')}}">Servicios</a></li>
                                        <li><a href="{{url('/galeria')}}">Galeria</a></li>

                                        <li><a href="{{url('/preguntas')}}">Preguntas Frecuentes</a></li>
                                        <li><a href="{{url('/contacto')}}">Contactos</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-2 d-none d-lg-block">
                            <div class="log_chat_area d-flex align-items-end">
                                <a href="#" data-scroll-nav="0" class="say_hi">Inicio</a>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->
    <!-- slider_area_start -->
    @yield('pages')
    <!-- footer start -->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-md-3">
                        <div class="footer_logo wow fadeInRight" data-wow-duration="1s" data-wow-delay=".3s">
                            <a href="index.html">
                                <img src="{{ asset('visit/img/animania.png')}}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-9">
                        <div class="menu_links">
                            <ul>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s" href="{{url('/')}}">Inicio</a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s"></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".4s" href="{{url('/servicios')}}">Servicios</a></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".6s" href="{{url('/galeria')}}">Galeria</a></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".8s" href="{{url('/preguntas')}}">Preguntas Frecuentes</a></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1.1s" href="{{url('/contacto')}}">Contactanos</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="socail_links">
                            <ul>
                                <li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" href="https://www.facebook.com/animanialuna/"> <i class="fa fa-facebook"></i> </a></li>
                                <!--<li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" href="#"> <i class="fa fa-twitter"></i> </a></li>-->
                                <li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" href="#"> <i class="fa fa-instagram"></i> </a></li>
                               <!-- <li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" href="#"> <i class="fa fa-google-plus"></i> </a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Ing.Luis Enrique Martinez Avila
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->
    <!-- JS here -->
    <script src="{{ asset('visit/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{ asset('visit/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{ asset('visit/js/popper.min.js')}}"></script>
    <script src="{{ asset('visit/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('visit/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('visit/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('visit/js/ajax-form.js')}}"></script>
    <script src="{{ asset('visit/js/waypoints.min.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.counterup.min.js')}}"></script>
    <script src="{{ asset('visit/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('visit/js/scrollIt.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{ asset('visit/js/wow.min.js')}}"></script>
    <script src="{{ asset('visit/js/nice-select.min.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('visit/js/plugins.js')}}"></script>
    <script src="{{ asset('visit/js/gijgo.min.js')}}"></script>
    <!--contact js-->
    <script src="{{ asset('visit/js/contact.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.form.js')}}"></script>
    <script src="{{ asset('visit/js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('visit/js/mail-script.js')}}"></script>
    <script src="{{ asset('visit/js/main.js')}}"></script>
</body>
</html>
