<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style>
        h1{
        text-align: center;
        text-transform: uppercase;
        }
        .contenido{
        font-size: 20px;
        }
        #primero{
        background-color: #ccc;
        }
        #segundo{
        color:#44a359;
        }
        #tercero{
        text-decoration:line-through;
        }
    </style>
    </head>
    <body>


        <h1>
           Contratos
       </h1> 
       <br/>
       <h2>
            Documento de Presentacion de Servicios Digitales
       </h2>       
       <p>
           Conste con el presente contrato de servicios musicales 
           los abajo suscritos a lo que nos sometemos a las siguientes
           clauslas
       </p>
       <br/>
       <h3>
           Primero
       </h3>
       <p>
            Yo __________ con DNI __________ representante de __________
            contrato bajo responsabilidad los servicios del grupo musical 
            "LOS AMOS DEL RITMO ENLACE"
        </p>
        <p>
            para amenizar un(a) __________ en la Ciudad de __________ local
            __________ el dia __________ por espacio de horas __________, 
            apartir de __________ hrs a __________ hrs  
        </p>
        <br/>        
        <h3>
           Segundo
        </h3>
        <p>
            Yo VICTOR HUGO CESAR LINO con carnet de extranjero No 65416518 representante
            del grupo mencionado (ENLACE) me compremeto a asistir al compromiso pactado en 
            este documento, a la hora fijada con todos los integrantes e instrumental correspondiente 
        </p>
        <br/>        
             
        <h3>
           Tercero
        </h3>
        <p>
            El precio del contrato en acuerdo mutuo es la suma de 
            $ __________ (__________ MNX) dejando un adelanto de 
            $ __________ (__________ MNX) y un saldo de 
            $ __________ (__________ MNX) que será saldado antes de la presentacion 
            del grupo en escena
        </p>
        <br/>        
        <h3>
           Cuarto
        </h3>
        <p>
            Si por motivo alguno no se realiza la actividad en la fecha indicada
            el contratante perdera automaticamente el adelanto contratado
        </p>
        <br/>        
        <h3>
           Quinto
        </h3>
        <p>
            La estadia y alimentacion del grupo musical estará a cargo del
            __________
        </p>
        <br/>        
        <h3>
           Sexto
        </h3>
        <p>
            En caso de incumplimiento de una de las partes, se procederá
            legalmemte con las autoridades pertinentes
        </p>
        <br/>        
        <h3>
           Septimo
        </h3>
        <p>
            Estando ambos de acuerdo con el presente contrato, firmamos en __________
            a las __________ del dia __________ de __________ del presente año

        </p>
        <p>
            Oaxaca de Juarez; Oax a __________ de __________ del __________
        </p>
        <table>
            <tr>
                <th>Contratante</th>
                <th>Representante</th>
            </tr>
            <tr>
                <th>__________</th>
                <th>__________</th>                
            </tr>
        </table>
    </body>
</html>