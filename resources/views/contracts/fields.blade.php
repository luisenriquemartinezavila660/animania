<!-- Package Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('package_id', 'Package Id:') !!}
    {!! Form::number('package_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Celebrate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_celebrate', 'Name Celebrate:') !!}
    {!! Form::text('name_celebrate', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Celebrated Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_celebrated', 'Date Celebrated:') !!}
    {!! Form::date('date_celebrated', null, ['class' => 'form-control','id'=>'date_celebrated']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#date_celebrated').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Date Contract Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_contract', 'Date Contract:') !!}
    {!! Form::date('date_contract', null, ['class' => 'form-control','id'=>'date_contract']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#date_contract').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('contracts.index') }}" class="btn btn-default">Cancel</a>
</div>
