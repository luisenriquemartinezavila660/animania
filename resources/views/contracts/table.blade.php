<div class="table-responsive">
    <table class="table" id="contracts-table">
        <thead>
            <tr>
                <th>Package Id</th>
        <th>Name</th>
        <th>Address</th>
        <th>Name Celebrate</th>
        <th>Date Celebrated</th>
        <th>Date Contract</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($contracts as $contract)
            <tr>
                <td>{{ $contract->package_id }}</td>
            <td>{{ $contract->name }}</td>
            <td>{{ $contract->address }}</td>
            <td>{{ $contract->name_celebrate }}</td>
            <td>{{ $contract->date_celebrated }}</td>
            <td>{{ $contract->date_contract }}</td>
            <td>{{ $contract->status }}</td>
                <td>
                    {!! Form::open(['route' => ['contracts.destroy', $contract->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('contracts.show', [$contract->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('contracts.edit', [$contract->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
