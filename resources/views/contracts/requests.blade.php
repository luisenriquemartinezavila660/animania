@extends('layouts.app')

@section('content')
<div class="container row">
	<div class="col-md-12">
		<section class="row">
        <div class="wizard col-md-12">
            <div class="wizard-inner col-md-12">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form">
                <div class="tab-content col-md-12">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="row">
                            <div class="col-offset-md-4">
                                <h3>Datos Personales</h3>
                            </div>
                        </div>
                        
                        <label>Nombre Completo:</label>
                        <input type="text" class="form-control">
                        <label>edad:</label>
                        <input type="text" class="form-control">
                        <label>nombre del cumpleañero:</label>
                        <input type="text" class="form-control">
                        <label>Telefono Celular:</label>
                        <input type="textarea" class="form-control">
                        <label>fecha del evento:</label>
                        <input type="date" class="form-control">
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-primary next-step">Continuar</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <h3>Escoge a tu animador</h3>
                        <div>
                            <input type="radio" name="emotion" id="sad" class="input-hidden" />
                            <label for="sad">
                                <img src="//placekitten.com/150/150" alt="I'm sad" />
                            </label>
                            <input type="radio" name="emotion" id="happy" class="input-hidden" />
                            <label for="happy">
                                <img src="//placekitten.com/151/151" alt="I'm happy" />
                            </label>
                        </div>
                        <h3>escoge tus personajes</h3>
                        <div>
                            <input type="radio" name="emotion" id="sad" class="input-hidden" />
                            <label for="sad">
                                <img src="//placekitten.com/150/150" alt="I'm sad" />
                            </label>
                            <input type="radio" name="emotion" id="happy" class="input-hidden" />
                            <label for="happy">
                                <img src="//placekitten.com/151/151" alt="I'm happy" />
                            </label>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3>BUSCA LA DIRECCION DE TU EVENTO</h3>
                        <section class="contact-section section_padding">
    <div class="container col-12">
      <div class="d-none d-sm-block mb-5 pb-4">
        <div id="map" style="height: 480px;"></div>
        <script>
          function initMap() {
            var uluru = {lat: -25.363, lng: 131.044};
            var grayStyles = [
              {
                featureType: "all",
                stylers: [
                  { saturation: -90 },
                  { lightness: 50 }
                ]
              },
              {elementType: 'labels.text.fill', stylers: [{color: '#ccdee9'}]}
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: 17.050613, lng: -96.736861},
              zoom: 31,
              styles: grayStyles,
              scrollwheel:  true
            });
          }
          
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&callback=initMap"></script>
      </div>
    </div>
  </section>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <!--<li><button type="button" class="btn btn-default next-step">Skip</button></li>-->
                            <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">
                        <h3>Complete</h3>
                        <p>You have successfully completed all steps.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>
@endsection
