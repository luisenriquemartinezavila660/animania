<!-- Package Id Field -->
<div class="form-group">
    {!! Form::label('package_id', 'Package Id:') !!}
    <p>{{ $contract->package_id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $contract->name }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $contract->address }}</p>
</div>

<!-- Name Celebrate Field -->
<div class="form-group">
    {!! Form::label('name_celebrate', 'Name Celebrate:') !!}
    <p>{{ $contract->name_celebrate }}</p>
</div>

<!-- Date Celebrated Field -->
<div class="form-group">
    {!! Form::label('date_celebrated', 'Date Celebrated:') !!}
    <p>{{ $contract->date_celebrated }}</p>
</div>

<!-- Date Contract Field -->
<div class="form-group">
    {!! Form::label('date_contract', 'Date Contract:') !!}
    <p>{{ $contract->date_contract }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $contract->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $contract->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $contract->updated_at }}</p>
</div>

