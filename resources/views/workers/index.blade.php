@extends('layouts.app')

@section('content')
@if(\Auth::user()->can('Create'))

<section class="content-header">
    <h1 class="pull-right">
       <a class="btn btn-block btn-primary btn-lg" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('workers.create') }}">Añadir Nuevo Trabajador</a>
    </h1>
</section>
@endif
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('workers.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

