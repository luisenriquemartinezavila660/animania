<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre Completo:') !!}
    <p>{{ $worker->name }}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Edad:') !!}
    <p>{{ $worker->age }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'direccion:') !!}
    <p>{{ $worker->address }}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telefono:') !!}
    <p>{{ $worker->telephone }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Correo:') !!}
    <p>{{ $worker->email }}</p>
</div>

<!-- Post Id Field -->
<div class="form-group">
    {!! Form::label('post_id', 'Puestos:') !!}
    <p>{{ $worker->post_id }}</p>
</div>


