<!-- Name Field -->
<div class="row">
    <div class="form-group col-sm-9">
        {!! Form::label('name', 'Nombre Completo:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Age Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('age', 'Edad:') !!}
        {!! Form::text('age', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Address Field -->
<div class="form-group col-sm-12">
    {!! Form::label('address', 'Direccion:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>
<div class="row">
    <div class="form-group col-sm-3">
        {!! Form::label('telephone', 'Telefono:') !!}
        {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-9">
        {!! Form::label('email', 'Correo:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
</div>
<!-- Telephone Field -->


<div class="form-group col-sm-6">
    {!! Form::label('post', 'Puestos de Trabajo:') !!}<br>
@foreach($posts as $post)
    {{Form::checkbox('post_id[]',$post->id,(isset($worker)?($worker->posts->find($post->id)?true:false):false),['id'=>'$post->id', 'class'=>'uk-checkbox '])}} {{$post->name}}<h6>    </h6>
@endforeach
</div>

<!-- Submit Field -->
<div class="row">
    <div class="offset-md-3 col-md-2">
        <a href="{!! route('workers.index') !!}" class="btn btn-block bg-gradient-danger btn-flat">
            <i class="fas fa-reply"></i>
            <p>Atras</p>
        </a>
    </div>
    <div class="col-md-2">
        <button class="btn btn-block bg-gradient-success btn-flat " type="submit">
            <i class="  fas fa-save"></i>
            <p>Guardar</p>
        </button>
    </div>
</div>
