<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('animators','AnimatorController@getAnimators');
Route::get('characters','CharacterController@getCharacters');
Route::post('person_data','ContractController@personData');
Route::post('location_data','LocationController@locationData');
Route::get('events','EventController@allEvents');
Route::post('event','EventController@saveEvent');
Route::post('save_animators','AnimatorController@saveAnimators');
Route::post('save_characters','CharacterController@saveCharacters');
Route::get('get_calculations','ContractController@getCalculations');
