<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//pagina de visitas
Route::get('/','FrontController@index');
Route::get('/servicios','FrontController@service');
Route::get('/galeria','FrontController@galery');
Route::get('/preguntas','FrontController@question');
Route::get('/contacto','FrontController@contact');
Auth::routes(['verify' => true]);
//inventario
Route::resource('garments', 'GarmentController');
Route::resource('discounts', 'DiscountController'); //
Route::resource('suits', 'SuitController');
Route::resource('motleys', 'MotleyController');
Route::resource('packages', 'PackageController');
Route::resource('characters', 'CharacterController');
Route::resource('props', 'PropController');//
//administracion
Route::get('/home', 'HomeController@index')->middleware('verified');
Route::get('/01010100_01100101_00100000_01100001_01101101_01101111_00100000_01001101_01100001_01110010_01101001_01110011_01101111_01101100','HomeController@secret');
Route::resource('animators', 'AnimatorController');//
Route::resource('workers', 'WorkerController');
Route::resource('posts', 'PostController');
//administrar categorias
Route::resource('categoryCharacters', 'CategoryCharacterController');
Route::resource('categoryProps', 'CategoryPropController');
Route::resource('categoryGarments', 'CategoryGarmentController');
//rutas de prueba
//Route::get('/contrato', 'PruebasController@contract');
Route::resource('contracts', 'ContractController');
Route::resource('locations', 'LocationController');
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('cotizacion','WizardController@getWizard');
/////////////////////////////PRUEBAS //////////////////////////////////////////////////////////
Route::name('imprimir')->get('/imprimir-pdf','WizardController@downPdf');
Route::get('pruebas','ContractController@getCalculations');



Route::resource('events', 'EventController');