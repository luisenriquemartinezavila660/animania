<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Discount
 * @package App\Models
 * @version March 28, 2020, 5:32 am UTC
 *
 * @property string name
 * @property integer percentage
 */
class Discount extends Model
{
    use SoftDeletes;

    public $table = 'discounts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'percentage'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'percentage' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'percentage' => 'required'
    ];

    
}
