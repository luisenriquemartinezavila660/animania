<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CategoryProp
 * @package App\Models
 * @version March 28, 2020, 7:36 am UTC
 *
 * @property string name
 * @property string description
 */
class CategoryProp extends Model
{
    use SoftDeletes;

    public $table = 'category_props';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

        public function props(){
            return $this->hasMany('App\Models\Prop');
        }
    
}
