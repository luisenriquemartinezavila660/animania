<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Package
 * @package App\Models
 * @version March 28, 2020, 7:56 am UTC
 *
 * @property string name
 * @property integer motley_id
 * @property integer worker_id
 * @property integer character_id
 */
class Package extends Model
{
    use SoftDeletes;

    public $table = 'packages';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'motley_id',
        'worker_id',
        'character_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'motley_id' => 'integer',
        'worker_id' => 'integer',
        'character_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'motley_id' => 'required',
        'worker_id' => 'required',
        'character_id' => 'required'
    ];
    public function contracts()
    {
        return $this->hasMany('App\Models\Contract');
    }
    
}
