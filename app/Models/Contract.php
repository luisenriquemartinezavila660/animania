<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contract
 * @package App\Models
 * @version April 17, 2020, 10:57 pm UTC
 *
 * @property integer package_id
 * @property string name
 * @property string address
 * @property string name_celebrate
 * @property string date_celebrated
 * @property string date_contract
 * @property integer status
 */
class Contract extends Model
{
    use SoftDeletes;

    public $table = 'contracts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'invoice',
        'event_id',
        'location_id',
        'package_id',
        'name',
        'telephone',
        'address',
        'name_celebrate',
        'type_event',
        'age_celebrated',
        'date_celebrated',
        'date_contract',
        
        'total',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'package_id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'name_celebrate' => 'string',
        'date_celebrated' => 'date',
        'date_contract' => 'date',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'package_id' => 'required',
        'name' => 'required',
        'address' => 'required',
        'name_celebrate' => 'required',
        'date_celebrated' => 'required',
        'date_contract' => 'required',
        'status' => 'required'
    ];

    public function Animators()
    {
        return $this->belongsToMany('App\Models\Animator')->withTimestamps();
    }
    public function Posts()
    {
        return $this->belongsToMany('App\Models\Post')->withTimestamps();
    }
    public function Characters(){
        return $this->belongsToMany('App\Models\Character')->withTimestamps();
    }
    public function Packages()
    {
        return $this->belongsTo('App\Models\Package');
    }
    public function location(){
        return $this->belongsTo('App\Models\Location');
    }
    public function event(){
        return $this->belongsTo('App\Models\Event');
    }
}
