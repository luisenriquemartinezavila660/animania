<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Garment
 * @package App\Models
 * @version March 28, 2020, 7:41 am UTC
 *
 * @property string name
 * @property integer type_garment
 * @property integer stock
 */
class Garment extends Model
{
    use SoftDeletes;

    public $table = 'garments';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'stock',
        'category_garment_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'stock' => 'integer',
        'category_garment_id'=>'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'stock' => 'required',
        'category_garment_id'=>'required'
    ];
    public function categoryGarments()
    {
        return $this->belongsTo('App\Models\CategoryGarment','category_garment_id');
    }
    public function characters(){
        return $this->belongsToMany('App\Models\Character');
    }

}
