<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Suit
 * @package App\Models
 * @version March 28, 2020, 7:49 am UTC
 *
 * @property string character
 * @property integer garment_id
 * @property integer stock
 * @property integer status
 */
class Suit extends Model
{
    use SoftDeletes;

    public $table = 'suits';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'character',
        'garment_id',
        'stock',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'character' => 'string',
        'garment_id' => 'integer',
        'stock' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'character' => 'required',
        'garment_id' => 'required',
        'stock' => 'required',
        'status' => 'required'
    ];

    
}
