<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Prop
 * @package App\Models
 * @version March 28, 2020, 5:41 am UTC
 *
 * @property string name
 * @property integer category_prop_id
 * @property integer stock
 */
class Prop extends Model
{
    use SoftDeletes;

    public $table = 'props';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'category_prop_id',
        'stock'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'category_prop_id' => 'integer',
        'stock' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'category_prop_id' => 'required',
        'stock' => 'required'
    ];
    public function categoryProp()
    {
        return $this->belongsTo('App\Models\CategoryProp','category_prop_id');
    }

}
