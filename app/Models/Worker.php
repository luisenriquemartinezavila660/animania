<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Worker
 * @package App\Models
 * @version March 28, 2020, 7:46 am UTC
 *
 * @property string name
 * @property string age
 * @property string address
 * @property string telephone
 * @property string email
 * @property integer post_id
 * @property integer status
 */
class Worker extends Model
{
    use SoftDeletes;

    public $table = 'workers';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'age',
        'address',
        'telephone',
        'email',
        //'post_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'age' => 'string',
        'address' => 'string',
        'telephone' => 'string',
        'email' => 'string',
        //'post_id' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'age' => 'required',
        'address' => 'required',
        'telephone' => 'required',
        'email' => 'required',
        'post_id' => 'required'
    ];
    public function Posts()
    {
        return $this->belongsToMany(Post::class)->withTimestamps();
    }


}
