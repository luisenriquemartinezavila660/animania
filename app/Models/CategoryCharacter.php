<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CategoryCharacter
 * @package App\Models
 * @version March 28, 2020, 5:48 am UTC
 *
 * @property string name
 * @property string description
 */
class CategoryCharacter extends Model
{
    use SoftDeletes;

    public $table = 'category_characters';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];
    public function characters(){
        return $this->hasMany('App\Models\Character');
    }
    
}
