<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Motley
 * @package App\Models
 * @version March 28, 2020, 7:52 am UTC
 *
 * @property string character
 * @property string source
 * @property integer stock
 * @property integer status
 * @property number cost
 */
class Motley extends Model
{
    use SoftDeletes;

    public $table = 'motleys';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'character',
        'source',
        'stock',
        'status',
        'cost'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'character' => 'string',
        'source' => 'string',
        'stock' => 'integer',
        'status' => 'integer',
        'cost' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'character' => 'required',
        'source' => 'required',
        'stock' => 'required',
        'status' => 'required',
        'cost' => 'required'
    ];

    
}
