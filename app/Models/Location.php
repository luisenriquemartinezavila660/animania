<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 * @package App\Models
 * @version May 1, 2021, 7:27 am UTC
 *
 * @property number $latitude
 * @property number $length
 */
class Location extends Model
{
    use SoftDeletes;

    public $table = 'locations';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'latitude',
        'length',
        'references',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'latitude' => 'float',
        'length' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function contract(){
        return $this->hasOne('App\Models\Contract')->withTimestamps();
    }

    
}
