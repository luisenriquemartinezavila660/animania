<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Animator
 * @package App\Models
 * @version March 28, 2020, 5:47 am UTC
 *
 * @property string name
 * @property string photo
 * @property string experience
 * @property string telephone
 * @property string email
 */
class Animator extends Model
{
    use SoftDeletes;

    public $table = 'animators';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'photo',
        'experience',
        'telephone',
        'email',
        'cost'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'photo' => 'string',
        'experience' => 'string',
        'telephone' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'experience' => 'required',
        'telephone' => 'required',
        'email' => 'required',
        'cost' => 'required'
    ];
    public function Contracts()
    {
        return $this->belongsToMany('App\Models\Contract')->withTimestamps();
    }

}
