<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Character
 * @package App\Models
 * @version March 28, 2020, 7:57 am UTC
 *
 * @property string name
 * @property string source
 * @property string photo
 * @property integer status
 * @property number cost
 */
class Character extends Model
{
    use SoftDeletes;

    public $table = 'characters';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'source',
        'photo',
        'status',
        'cost',
        'character_category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'source' => 'string',
        'photo' => 'string',
        'status' => 'integer',
        'cost' => 'float',
        'character_category_id'=>'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'source' => 'required',

        'cost' => 'required',
        'character_category_id' =>'required'
    ];
    public function CategoryCharacters(){
        return $this->belongsTo('App\Models\CategoryCharacter','character_category_id');
    }
    public function garments(){
        return $this->belongsToMany('App\Models\Garment')->withTimestamps();
    }
    public function Contracts(){
        return $this->belongsToMany('App\Models\Contract')->withTimestamps();
    }


}
