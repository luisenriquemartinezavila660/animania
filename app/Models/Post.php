<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Models
 * @version March 28, 2020, 7:40 am UTC
 *
 * @property string name
 * @property string description
 * @property number cost
 */
class Post extends Model
{
    use SoftDeletes;

    public $table = 'posts';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'cost'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'cost' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required',
        'cost' => 'required'
    ];
    public function Workers()
    {
        return $this->belongsToMany('App\Models\Worker')->withTimestamps();
    }
    public function Contracts()
    {
        return $this->belongsToMany('App\Models\Contract')->withTimestamps();
    }

}
