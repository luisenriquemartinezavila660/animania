<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryPropRequest;
use App\Http\Requests\UpdateCategoryPropRequest;
use App\Repositories\CategoryPropRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryPropController extends AppBaseController
{
    /** @var  CategoryPropRepository */
    private $categoryPropRepository;

    public function __construct(CategoryPropRepository $categoryPropRepo)
    {
        $this->categoryPropRepository = $categoryPropRepo;
    }

    /**
     * Display a listing of the CategoryProp.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryProps = $this->categoryPropRepository->all();

        return view('category_props.index')
            ->with('categoryProps', $categoryProps);
    }

    /**
     * Show the form for creating a new CategoryProp.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_props.create');
    }

    /**
     * Store a newly created CategoryProp in storage.
     *
     * @param CreateCategoryPropRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryPropRequest $request)
    {
        $input = $request->all();

        $categoryProp = $this->categoryPropRepository->create($input);

        Flash::success('La Categoria se guardo con Exito.');

        return redirect(route('categoryProps.index'));
    }

    /**
     * Display the specified CategoryProp.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryProp = $this->categoryPropRepository->find($id);

        if (empty($categoryProp)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryProps.index'));
        }

        return view('category_props.show')->with('categoryProp', $categoryProp);
    }

    /**
     * Show the form for editing the specified CategoryProp.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryProp = $this->categoryPropRepository->find($id);

        if (empty($categoryProp)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryProps.index'));
        }

        return view('category_props.edit')->with('categoryProp', $categoryProp);
    }

    /**
     * Update the specified CategoryProp in storage.
     *
     * @param int $id
     * @param UpdateCategoryPropRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryPropRequest $request)
    {
        $categoryProp = $this->categoryPropRepository->find($id);

        if (empty($categoryProp)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryProps.index'));
        }

        $categoryProp = $this->categoryPropRepository->update($request->all(), $id);

        Flash::success('La Categoria se Actualizo con Exito.');

        return redirect(route('categoryProps.index'));
    }

    /**
     * Remove the specified CategoryProp from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryProp = $this->categoryPropRepository->find($id);

        if (empty($categoryProp)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryProps.index'));
        }

        $this->categoryPropRepository->delete($id);

        Flash::success('La Categoria se Elimino con Exito.');

        return redirect(route('categoryProps.index'));
    }
}
