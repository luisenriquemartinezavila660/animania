<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGarmentRequest;
use App\Http\Requests\UpdateGarmentRequest;
use App\Repositories\GarmentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\CategoryGarment;
use Flash;
use Response;

class GarmentController extends AppBaseController
{
    /** @var  GarmentRepository */
    private $garmentRepository;

    public function __construct(GarmentRepository $garmentRepo)
    {
        $this->garmentRepository = $garmentRepo;
    }

    /**
     * Display a listing of the Garment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $garments = $this->garmentRepository->all();

        return view('garments.index')
            ->with('garments', $garments);
    }

    /**
     * Show the form for creating a new Garment.
     *
     * @return Response
     */
    public function create()
    {
        $categories = CategoryGarment::pluck('name','id');
        return view('garments.create')->with('categories',$categories);
    }

    /**
     * Store a newly created Garment in storage.
     *
     * @param CreateGarmentRequest $request
     *
     * @return Response
     */
    public function store(CreateGarmentRequest $request)
    {
        $input = $request->all();

        $garment = $this->garmentRepository->create($input);

        Flash::success('Prenda guardada con éxito.');

        return redirect(route('garments.index'));
    }

    /**
     * Display the specified Garment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $garment = $this->garmentRepository->find($id);

        if (empty($garment)) {
            Flash::error('Garment not found');

            return redirect(route('garments.index'));
        }

        return view('garments.show')->with('garment', $garment);
    }

    /**
     * Show the form for editing the specified Garment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = CategoryGarment::pluck('name','id');
        $garment = $this->garmentRepository->find($id);

        if (empty($garment)) {
            Flash::error('Garment not found');

            return redirect(route('garments.index'));
        }

        return view('garments.edit')->with('garment', $garment)->with('categories',$categories);
    }

    /**
     * Update the specified Garment in storage.
     *
     * @param int $id
     * @param UpdateGarmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGarmentRequest $request)
    {
        $garment = $this->garmentRepository->find($id);

        if (empty($garment)) {
            Flash::error('Garment not found');

            return redirect(route('garments.index'));
        }

        $garment = $this->garmentRepository->update($request->all(), $id);

        Flash::success('Prenda actualizada con éxito.');

        return redirect(route('garments.index'));
    }

    /**
     * Remove the specified Garment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $garment = $this->garmentRepository->find($id);

        if (empty($garment)) {
            Flash::error('Garment not found');

            return redirect(route('garments.index'));
        }

        $this->garmentRepository->delete($id);

        Flash::success('Prenda eliminada con éxito.');

        return redirect(route('garments.index'));
    }
}
