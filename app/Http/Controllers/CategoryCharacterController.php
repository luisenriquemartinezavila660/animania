<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryCharacterRequest;
use App\Http\Requests\UpdateCategoryCharacterRequest;
use App\Repositories\CategoryCharacterRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryCharacterController extends AppBaseController
{
    /** @var  CategoryCharacterRepository */
    private $categoryCharacterRepository;

    public function __construct(CategoryCharacterRepository $categoryCharacterRepo)
    {
        $this->categoryCharacterRepository = $categoryCharacterRepo;
    }

    /**
     * Display a listing of the CategoryCharacter.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryCharacters = $this->categoryCharacterRepository->all();

        return view('category_characters.index')
            ->with('categoryCharacters', $categoryCharacters);
    }

    /**
     * Show the form for creating a new CategoryCharacter.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_characters.create');
    }

    /**
     * Store a newly created CategoryCharacter in storage.
     *
     * @param CreateCategoryCharacterRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryCharacterRequest $request)
    {
        $input = $request->all();

        $categoryCharacter = $this->categoryCharacterRepository->create($input);

        Flash::success('La Categoria se guardo con Exito.');

        return redirect(route('categoryCharacters.index'));
    }

    /**
     * Display the specified CategoryCharacter.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryCharacter = $this->categoryCharacterRepository->find($id);

        if (empty($categoryCharacter)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryCharacters.index'));
        }

        return view('category_characters.show')->with('categoryCharacter', $categoryCharacter);
    }

    /**
     * Show the form for editing the specified CategoryCharacter.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryCharacter = $this->categoryCharacterRepository->find($id);

        if (empty($categoryCharacter)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryCharacters.index'));
        }

        return view('category_characters.edit')->with('categoryCharacter', $categoryCharacter);
    }

    /**
     * Update the specified CategoryCharacter in storage.
     *
     * @param int $id
     * @param UpdateCategoryCharacterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryCharacterRequest $request)
    {
        $categoryCharacter = $this->categoryCharacterRepository->find($id);

        if (empty($categoryCharacter)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryCharacters.index'));
        }

        $categoryCharacter = $this->categoryCharacterRepository->update($request->all(), $id);

        Flash::success('La Categoria fue Actualizada con Exito.');

        return redirect(route('categoryCharacters.index'));
    }

    /**
     * Remove the specified CategoryCharacter from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryCharacter = $this->categoryCharacterRepository->find($id);

        if (empty($categoryCharacter)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryCharacters.index'));
        }

        $this->categoryCharacterRepository->delete($id);

        Flash::success('La Categoria fue Eliminada con Exito.');

        return redirect(route('categoryCharacters.index'));
    }
}
