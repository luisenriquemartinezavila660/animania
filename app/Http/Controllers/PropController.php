<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePropRequest;
use App\Http\Requests\UpdatePropRequest;
use App\Repositories\PropRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\CategoryProp;
use Flash;
use Response;

class PropController extends AppBaseController
{
    /** @var  PropRepository */
    private $propRepository;

    public function __construct(PropRepository $propRepo)
    {
        $this->propRepository = $propRepo;
    }

    /**
     * Display a listing of the Prop.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $props = $this->propRepository->all();

        return view('props.index')
            ->with('props', $props);
    }

    /**
     * Show the form for creating a new Prop.
     *
     * @return Response
     */
    public function create()
    {
        $categories = CategoryProp::pluck('name','id');
        return view('props.create')->with('categories',$categories);
    }

    /**
     * Store a newly created Prop in storage.
     *
     * @param CreatePropRequest $request
     *
     * @return Response
     */
    public function store(CreatePropRequest $request)
    {
        $input = $request->all();

        $prop = $this->propRepository->create($input);

        Flash::success('Utileria guardado correctamente.');

        return redirect(route('props.index'));
    }

    /**
     * Display the specified Prop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prop = $this->propRepository->find($id);

        if (empty($prop)) {
            Flash::error('Prop not found');

            return redirect(route('props.index'));
        }

        return view('props.show')->with('prop', $prop);
    }

    /**
     * Show the form for editing the specified Prop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = CategoryProp::pluck('name','id');
        $prop = $this->propRepository->find($id);

        if (empty($prop)) {
            Flash::error('Prop not found');

            return redirect(route('props.index'));
        }

        return view('props.edit')->with('prop', $prop)->with('categories',$categories);
    }

    /**
     * Update the specified Prop in storage.
     *
     * @param int $id
     * @param UpdatePropRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropRequest $request)
    {
        $prop = $this->propRepository->find($id);

        if (empty($prop)) {
            Flash::error('Prop not found');

            return redirect(route('props.index'));
        }

        $prop = $this->propRepository->update($request->all(), $id);

        Flash::success('Utileria actualizada correctamente.');

        return redirect(route('props.index'));
    }

    /**
     * Remove the specified Prop from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prop = $this->propRepository->find($id);

        if (empty($prop)) {
            Flash::error('Prop not found');

            return redirect(route('props.index'));
        }

        $this->propRepository->delete($id);

        Flash::success('Utileria eliminada correctamente.');

        return redirect(route('props.index'));
    }
}
