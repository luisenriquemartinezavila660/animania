<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCharacterRequest;
use App\Http\Requests\UpdateCharacterRequest;
use App\Repositories\CharacterRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CategoryCharacterRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\CategoryCharacter;
use App\Models\Character;
use App\Models\Contract;
use Illuminate\Support\Facades\Storage;


class CharacterController extends AppBaseController
{
    /** @var  CharacterRepository */
    private $characterRepository;
    private $categoryCharacterRepository;

    public function __construct(CharacterRepository $characterRepo,CategoryCharacterRepository $categoryRepo)
    {
        $this->characterRepository = $characterRepo;
        $this->categoryCharacterRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Character.
     *
     * @param Request $request
     *
     * @return Response
     */
    //#################################API METHODS###############################################################
    public function getCharacters(){
        $characters = Character::with('CategoryCharacters')->get();
        return response()->json($characters, 201);
    }

    public function saveCharacters(Request $request){
        $data = $request->all();
        
        $register = Contract::where('invoice','=',$data["invoice"])->limit(1)->get();
        
        $contract = Contract::find($register[0]->id);
        $contract->Characters()->attach($data["characters_id"]);
        return $contract->Characters()->get();
    }
    //####################################END API############################################################
    public function index(Request $request)
    {
        $characters = $this->characterRepository->all();



        return view('characters.index')
            ->with('characters', $characters);
    }

    /**
     * Show the form for creating a new Character.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $categories= CategoryCharacter::pluck('name','id');
        return view('characters.create')
                ->with('categories',$categories);
    }

    /**
     * Store a newly created Character in storage.
     *
     * @param CreateCharacterRequest $request
     *
     * @return Response
     */
    public function store(CreateCharacterRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('photo')){
            $input['photo']=$request->file('photo')->store('uploads','public');
        }


        $character = $this->characterRepository->create($input);


        Flash::success('Personaje guardado con éxito.');
        return redirect(route('characters.index'));
    }

    /**
     * Display the specified Character.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $character = $this->characterRepository->find($id);

        if (empty($character)) {
            Flash::error('Personaje no encontrado');

            return redirect(route('characters.index'));
        }

        return view('characters.show')->with('character', $character);
    }

    /**
     * Show the form for editing the specified Character.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories= CategoryCharacter::pluck('name','id');
        $character = $this->characterRepository->find($id);


        if (empty($character)) {
            Flash::error('Personaje no encontrado');

            return redirect(route('characters.index'));
        }

        return view('characters.edit')->with('character', $character)->with('categories',$categories);
    }

    /**
     * Update the specified Character in storage.
     *
     * @param int $id
     * @param UpdateCharacterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCharacterRequest $request)
    {
        $character= Character::find($id);
        $character->name=$request->input('name');
        $character->source=$request->input('source');
        $character->status=1;
        $character->photo = $character->photo;
        $character->cost=$request->input('cost');
        $character->character_category_id=$request->input('character_category_id');
        if($request->hasFile('photo')){
            $character = Character::findOrFail($id);
            $nombre = $character['photo'];
            Storage::delete('public/'.$nombre);
            $character->photo=$request->file('photo')->store('uploads','public');
        }
        if (empty($character)) {
            Flash::error('Personaje no encontrado');
            return redirect(route('characters.index'));
        }
        Flash::success('Personaje actualizado con éxito.');
        $character->save();
        return redirect(route('characters.index'));
    }

    /**
     * Remove the specified Character from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $character = $this->characterRepository->find($id);
        Storage::delete('public/'.$character['photo']);
            if (empty($character)) {
                Flash::error('Personaje no encontrado');

                return redirect(route('characters.index'));
            }

            $this->characterRepository->delete($id);

            Flash::success('Personaje eliminado con éxito.');

            return redirect(route('characters.index'));



    }
}
