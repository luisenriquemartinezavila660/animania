<?php

namespace App\Http\Controllers;
use App\Http\Requests\RoleRequest;
use App\Libraries\Factory\AbstractFactory;
use App\Permission;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $factory;
    private $dao;
    public function __construct(){
        $this->factory=AbstractFactory::getFactory('DAO');
        $this->dao=$this->factory->getDAO('RoleDao');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles= $this->dao->select();
        return view('roles.index')->with('roles',$roles);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions= Permission::all();
        return view('roles.create')->with('permissions',$permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->dao->insert($request->all());
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = Permission::get();
        $role=$this->dao->get($id);
        //dd($role);
        return view('roles.edit')->with('role',$role)->with('permissions',$permissions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->dao->update($request->all(),$id);
        return redirect('/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->dao->delete($id);
        return redirect('/roles');
    }
}
