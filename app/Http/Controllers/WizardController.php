<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Animator;
use App\Models\Contract;
use App\Models\Character;
use App\Models\Event;
use App\Models\Location;

class WizardController extends Controller
{
    //#################################API METHODS###############################################################
    
    //####################################END API############################################################
    public function getWizard(){
        $animators = Animator::all();
        //dd($animators);
        return view('front.wizard')->with('animators',$animators);
    }
    
    public function downPdf(){
        $pdf = \PDF::loadView('pruebas.pdf');
        ##return $pdf->download('ejemplo.pdf');
        return $pdf->stream();
    }
}
