<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryGarmentRequest;
use App\Http\Requests\UpdateCategoryGarmentRequest;
use App\Repositories\CategoryGarmentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryGarmentController extends AppBaseController
{
    /** @var  CategoryGarmentRepository */
    private $categoryGarmentRepository;

    public function __construct(CategoryGarmentRepository $categoryGarmentRepo)
    {
        $this->categoryGarmentRepository = $categoryGarmentRepo;
    }

    /**
     * Display a listing of the CategoryGarment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryGarments = $this->categoryGarmentRepository->all();

        return view('category_garments.index')
            ->with('categoryGarments', $categoryGarments);
    }

    /**
     * Show the form for creating a new CategoryGarment.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_garments.create');
    }

    /**
     * Store a newly created CategoryGarment in storage.
     *
     * @param CreateCategoryGarmentRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryGarmentRequest $request)
    {
        $input = $request->all();

        $categoryGarment = $this->categoryGarmentRepository->create($input);

        Flash::success('La Categoria se Guardo con Exito.');

        return redirect(route('categoryGarments.index'));
    }

    /**
     * Display the specified CategoryGarment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryGarment = $this->categoryGarmentRepository->find($id);

        if (empty($categoryGarment)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryGarments.index'));
        }

        return view('category_garments.show')->with('categoryGarment', $categoryGarment);
    }

    /**
     * Show the form for editing the specified CategoryGarment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryGarment = $this->categoryGarmentRepository->find($id);

        if (empty($categoryGarment)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryGarments.index'));
        }

        return view('category_garments.edit')->with('categoryGarment', $categoryGarment);
    }

    /**
     * Update the specified CategoryGarment in storage.
     *
     * @param int $id
     * @param UpdateCategoryGarmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryGarmentRequest $request)
    {
        $categoryGarment = $this->categoryGarmentRepository->find($id);

        if (empty($categoryGarment)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryGarments.index'));
        }

        $categoryGarment = $this->categoryGarmentRepository->update($request->all(), $id);

        Flash::success('La Categoria se Actualizo con Exito.');

        return redirect(route('categoryGarments.index'));
    }

    /**
     * Remove the specified CategoryGarment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryGarment = $this->categoryGarmentRepository->find($id);

        if (empty($categoryGarment)) {
            Flash::error('Categoria no encontrada');

            return redirect(route('categoryGarments.index'));
        }

        $this->categoryGarmentRepository->delete($id);

        Flash::success('La Categoria se Elimino con Exito.');

        return redirect(route('categoryGarments.index'));
    }
}
