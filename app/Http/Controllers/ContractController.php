<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateContractRequest;
use App\Http\Requests\UpdateContractRequest;
use App\Repositories\ContractRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Contract;
use App\Models\Animator;
use App\Models\Character;
use App\Models\Event;
use App\Models\Location;
use Flash;
use Response;

class ContractController extends AppBaseController
{
    /** @var  ContractRepository */
    private $contractRepository;

    public function __construct(ContractRepository $contractRepo)
    {
        $this->contractRepository = $contractRepo;
    }

    /**
     * Display a listing of the Contract.
     *
     * @param Request $request
     *
     * @return Response
     */
    //#################################API METHODS###############################################################
    public function personData(Request $request){
        $data = $request->all();
        //return $request->all();
        $personData = new Contract();
        //return $personData;
        $number=rand(100, 9999);
        //return $number;
        $personData->invoice =$number ;
        $personData->name =$data["name"];
        $personData->telephone=$data["telephone"];
        $personData->name_celebrate=$data["name_celebrate"];
        $personData->age_celebrated=$data["age_celebrated"];
        $personData->date_celebrated=$data["date_celebrated"];
        $personData->hour_celebrated=$data["hour_celebrated"];
        $personData->save();

        return response()->json($personData, 201);
    }
    public function getCalculations(Request $request){
        $data = $request->all();
        $totalCharacters =0;
        $totalAnimators =0;
        $register = Contract::where('invoice','=',$data['invoice'])->get();
        $prueba = $register->last();
        //hasta aqui regresa el invoice correcto para comenzar con los calculos 2947 con id 5
        //return response()->json($prueba->id, 201);
        $contract = Contract::find($prueba->id);
        //hasta aqui regresa el registro correcto pormedio de su id 5
        
        $listCharacters = $contract->Characters()->get();
        $listAnimators = $contract->Animators()->get();
        //hasta aqui todo bien, si regresa si id 5
        //return $contract->id;
        $event = $contract->event()->get();
        //return $contract->event()->get();
        foreach($listAnimators as $animator){
            $totalAnimators += $animator->cost;
        }
        foreach($listCharacters as $character){
            $totalCharacters += $character->cost;
            
        }
        
        $contract->total = $totalCharacters + $totalAnimators + $event[0]->cost;
        $contract->date_contract = new \DateTime();
        $contract->status = 1;
        $contract->save();
        $characters = $contract->Characters()->get();
        $animators= $contract->Animators()->get();
        $location = $contract->location()->get();
        //return $contract;
        //$contract = Contract::find($contract->id);
        $completContract = $contract;
        $conjunto =array('contract'=>$completContract,
        'event'=>$event,
        'location'=>$location,
        'characters'=> $characters,
        'animator'=>$animators,
        'totalAnimators'=>$totalAnimators,
        'totalCharacters'=>$totalCharacters);
        
        return response()->json($conjunto, 201);
        //return $totalCharacters;
        //return view('front.pruebas')->with('conjunto',$conjunto);
        
    }
    //####################################END API############################################################
    public function index(Request $request)
    {
        $contracts = $this->contractRepository->all();

        return view('contracts.index')
            ->with('contracts', $contracts);
    }

    /**
     * Show the form for creating a new Contract.
     *
     * @return Response
     */
    public function create()
    {
        return view('contracts.create');
    }

    /**
     * Store a newly created Contract in storage.
     *
     * @param CreateContractRequest $request
     *
     * @return Response
     */
    public function store(CreateContractRequest $request)
    {
        $input = $request->all();

        $contract = $this->contractRepository->create($input);

        Flash::success('Contract saved successfully.');

        return redirect(route('contracts.index'));
    }

    /**
     * Display the specified Contract.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error('Contract not found');

            return redirect(route('contracts.index'));
        }

        return view('contracts.show')->with('contract', $contract);
    }

    /**
     * Show the form for editing the specified Contract.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error('Contract not found');

            return redirect(route('contracts.index'));
        }

        return view('contracts.edit')->with('contract', $contract);
    }

    /**
     * Update the specified Contract in storage.
     *
     * @param int $id
     * @param UpdateContractRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContractRequest $request)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error('Contract not found');

            return redirect(route('contracts.index'));
        }

        $contract = $this->contractRepository->update($request->all(), $id);

        Flash::success('Contract updated successfully.');

        return redirect(route('contracts.index'));
    }

    /**
     * Remove the specified Contract from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error('Contract not found');

            return redirect(route('contracts.index'));
        }

        $this->contractRepository->delete($id);

        Flash::success('Contract deleted successfully.');

        return redirect(route('contracts.index'));
    }
}
