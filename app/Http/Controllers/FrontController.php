<?php

namespace App\Http\Controllers;

use App\Models\Animator;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        $animators=Animator::all();

        return view('front.index')
                ->with('animators',$animators);
    }
    public function service()
    {
        return view('front.service');

    }
    public function galery()
    {
        return view('front.galery');
    }
    public function question()
    {
        return view('front.questions');
    }
    public function contact()
    {
        return view('front.contact');
    }
    public function wizard(){
        return view('front.wizard');
    }
}
