<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMotleyRequest;
use App\Http\Requests\UpdateMotleyRequest;
use App\Repositories\MotleyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MotleyController extends AppBaseController
{
    /** @var  MotleyRepository */
    private $motleyRepository;

    public function __construct(MotleyRepository $motleyRepo)
    {
        $this->motleyRepository = $motleyRepo;
    }

    /**
     * Display a listing of the Motley.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $motleys = $this->motleyRepository->all();

        return view('motleys.index')
            ->with('motleys', $motleys);
    }

    /**
     * Show the form for creating a new Motley.
     *
     * @return Response
     */
    public function create()
    {
        return view('motleys.create');
    }

    /**
     * Store a newly created Motley in storage.
     *
     * @param CreateMotleyRequest $request
     *
     * @return Response
     */
    public function store(CreateMotleyRequest $request)
    {
        $input = $request->all();

        $motley = $this->motleyRepository->create($input);

        Flash::success('Motley saved successfully.');

        return redirect(route('motleys.index'));
    }

    /**
     * Display the specified Motley.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $motley = $this->motleyRepository->find($id);

        if (empty($motley)) {
            Flash::error('Motley not found');

            return redirect(route('motleys.index'));
        }

        return view('motleys.show')->with('motley', $motley);
    }

    /**
     * Show the form for editing the specified Motley.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $motley = $this->motleyRepository->find($id);

        if (empty($motley)) {
            Flash::error('Motley not found');

            return redirect(route('motleys.index'));
        }

        return view('motleys.edit')->with('motley', $motley);
    }

    /**
     * Update the specified Motley in storage.
     *
     * @param int $id
     * @param UpdateMotleyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMotleyRequest $request)
    {
        $motley = $this->motleyRepository->find($id);

        if (empty($motley)) {
            Flash::error('Motley not found');

            return redirect(route('motleys.index'));
        }

        $motley = $this->motleyRepository->update($request->all(), $id);

        Flash::success('Motley updated successfully.');

        return redirect(route('motleys.index'));
    }

    /**
     * Remove the specified Motley from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $motley = $this->motleyRepository->find($id);

        if (empty($motley)) {
            Flash::error('Motley not found');

            return redirect(route('motleys.index'));
        }

        $this->motleyRepository->delete($id);

        Flash::success('Motley deleted successfully.');

        return redirect(route('motleys.index'));
    }
}
