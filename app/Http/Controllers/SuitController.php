<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuitRequest;
use App\Http\Requests\UpdateSuitRequest;
use App\Repositories\SuitRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SuitController extends AppBaseController
{
    /** @var  SuitRepository */
    private $suitRepository;

    public function __construct(SuitRepository $suitRepo)
    {
        $this->suitRepository = $suitRepo;
    }

    /**
     * Display a listing of the Suit.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suits = $this->suitRepository->all();

        return view('suits.index')
            ->with('suits', $suits);
    }

    /**
     * Show the form for creating a new Suit.
     *
     * @return Response
     */
    public function create()
    {
        return view('suits.create');
    }

    /**
     * Store a newly created Suit in storage.
     *
     * @param CreateSuitRequest $request
     *
     * @return Response
     */
    public function store(CreateSuitRequest $request)
    {
        $input = $request->all();

        $suit = $this->suitRepository->create($input);

        Flash::success('Suit saved successfully.');

        return redirect(route('suits.index'));
    }

    /**
     * Display the specified Suit.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suit = $this->suitRepository->find($id);

        if (empty($suit)) {
            Flash::error('Suit not found');

            return redirect(route('suits.index'));
        }

        return view('suits.show')->with('suit', $suit);
    }

    /**
     * Show the form for editing the specified Suit.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suit = $this->suitRepository->find($id);

        if (empty($suit)) {
            Flash::error('Suit not found');

            return redirect(route('suits.index'));
        }

        return view('suits.edit')->with('suit', $suit);
    }

    /**
     * Update the specified Suit in storage.
     *
     * @param int $id
     * @param UpdateSuitRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuitRequest $request)
    {
        $suit = $this->suitRepository->find($id);

        if (empty($suit)) {
            Flash::error('Suit not found');

            return redirect(route('suits.index'));
        }

        $suit = $this->suitRepository->update($request->all(), $id);

        Flash::success('Suit updated successfully.');

        return redirect(route('suits.index'));
    }

    /**
     * Remove the specified Suit from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suit = $this->suitRepository->find($id);

        if (empty($suit)) {
            Flash::error('Suit not found');

            return redirect(route('suits.index'));
        }

        $this->suitRepository->delete($id);

        Flash::success('Suit deleted successfully.');

        return redirect(route('suits.index'));
    }
}
