<?php
namespace App\Http\Controllers;
use App\Http\Requests\CreateAnimatorRequest;
use App\Http\Requests\UpdateAnimatorRequest;
use App\Repositories\AnimatorRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Animator;
use App\Models\Contract;
use Illuminate\Http\Request;
/** clase de almacenamiento */
use Illuminate\Support\Facades\Storage;
/**
 * / clase de almacenamiento **/
use Flash;
use Illuminate\Contracts\Cache\Store;
use Response;
class AnimatorController extends AppBaseController
{
    /** @var  AnimatorRepository */
    private $animatorRepository;

    public function __construct(AnimatorRepository $animatorRepo)
    {
        $this->animatorRepository = $animatorRepo;
    }
    /**
     * Display a listing of the Animator.
     *
     * @param Request $request
     *
     * @return Response
     */
    //#################################API METHODS###############################################################
    public function getAnimators(){
        $animators = Animator::all();
        return response()->json($animators, 201);
    }
    public function saveAnimators(Request $request){
        $data = $request->all();
        $register = Contract::where('invoice','=',$data["invoice"])->limit(1)->get();
        $contract = Contract::find($register[0]->id);
        $contract->Animators()->attach($data["animator_id"]);
        return $contract->Animators()->get();
    }
    //####################################END API############################################################
    public function index(Request $request)
    {
        $animators = $this->animatorRepository->all();

        return view('animators.index')
            ->with('animators', $animators);
    }
    /**
     * Show the form for creating a new Animator.
     *
     * @return Response
     */
    public function create()
    {
        return view('animators.create');
    }
    /**
     * Store a newly created Animator in storage.
     *
     * @param CreateAnimatorRequest $request
     *
     * @return Response
     */
    public function store(CreateAnimatorRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('photo')){
            $input['photo']=$request->file('photo')->store('uploads','public');
        }

        $animator = $this->animatorRepository->create($input);


        Flash::success('
        animador guardado con éxito.');
        return redirect(route('animators.index'));
    }
    /**
     * Display the specified Animator.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $animator = $this->animatorRepository->find($id);
        if (empty($animator)) {
            Flash::error('Animator not found');
            return redirect(route('animators.index'));
        }
        return view('animators.show')->with('animator', $animator);
    }
    /**
     * Show the form for editing the specified Animator.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $animator = $this->animatorRepository->find($id);
        //dd($animator);
        if (empty($animator)) {
            Flash::error('Animator not found');
            return redirect(route('animators.index'));
        }
        return view('animators.edit')->with('animator', $animator);
    }
    /**
     * Update the specified Animator in storage.
     *
     * @param int $id
     * @param UpdateAnimatorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnimatorRequest $request)
    {
        $animator = $this->animatorRepository->find($id);
        $animator->name=$request->input('name');
        $animator->experience=$request->input('experience');
        //$animator->status=1;
        $animator->photo = $animator->photo;
        $animator->telephone=$request->input('telephone');
        $animator->email=$request->input('email');
        if($request->hasFile('photo')){
            $animator = Animator::findOrFail($id);
            $nombre = $animator['photo'];
            Storage::delete('public/'.$nombre);
            $animator->photo=$request->file('photo')->store('uploads','public');
        }
        if (empty($animator)) {
            Flash::error('Animator not found');
            return redirect(route('animators.index'));
        }
        //$animator = $this->animatorRepository->update($request->all(), $id);
        Flash::success('
        animador actualizado con éxito.');
        $animator->save();
        return redirect(route('animators.index'));
    }
    /**
     * Remove the specified Animator from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $animator = $this->animatorRepository->find($id);
        Storage::delete('public/'.$animator['photo']);
        if (empty($animator)) {
            Flash::error('Animator not found');
            return redirect(route('animators.index'));
        }
        $this->animatorRepository->delete($id);
        Flash::success('
        animador eliminado con éxito.');
        return redirect(route('animators.index'));
    }
}
