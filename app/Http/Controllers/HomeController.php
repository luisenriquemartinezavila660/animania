<?php

namespace App\Http\Controllers;
use App\Libraries\Factory\AbstractFactory;
use Illuminate\Http\Request;


use App\Models\Character;
class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $factory;
    private $dao;
    public function __construct()
    {
        $this->middleware('auth');
        $this->factory=AbstractFactory::getFactory('DAO');
        $this->dao=$this->factory->getDAO('HomeDao');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homes= $this->dao->get();
        //dd($homes);
        return view('home')->with('homes',$homes);
    }
    public function secret(){
        return view('love');
    }

    public function wizard(){
        $characters = Character::all();
        return view('wizard')->with('characters',$characters);
    }
}
