<?php

namespace App\Providers;
use App\Models\CategoryCharacter;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['characters.fields'], function($view){
            $categoryItems= CategoryCharacter::pluck('name','id')->toArray();
            $view->with('categoryItems',$categoryItems);
        });
    }
}
