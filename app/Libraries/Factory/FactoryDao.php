<?php
namespace App\Libraries\Factory;
use App\Libraries\Dao\UserDao;
use App\Libraries\Dao\RoleDao;
use App\Libraries\Dao\HomeDao;
class FactoryDAO
{
	public  function getDAO($dao)
	{
		if($dao=='UserDao'){
            return new UserDao();
        }
        if ($dao=='RoleDao') {
            return new RoleDao();
        }
        if($dao=='HomeDao'){
            return new HomeDao();
        }

        else{
            \abort(500,'dao no definido');
        }

	}

}
