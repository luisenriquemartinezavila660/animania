<?php
namespace App\Libraries\Factory;
class AbstractFactory{
    public static function getFactory($factory){
        if($factory=='DAO'){
            return new FactoryDao();
        }
        else{
            \abort(500,'fabrica no definida');
        }
    }
}
