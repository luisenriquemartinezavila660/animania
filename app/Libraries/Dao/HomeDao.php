<?php
namespace App\Libraries\Dao;
use Illuminate\Support\Facades\DB;
use App\Models\Character;
use App\Models\Animator;
use App\Models\Worker;
use App\Models\Contract;

class HomeDao
{
    public function get(){
        $characters= Character::count();
        $animators= Animator::count();
        $workers = Worker::count();
        $contracts=Contract::count();
        $homeCounts['characters']=$characters;
        $homeCounts['animators']=$animators;
        $homeCounts['workers']=$workers;
        $homeCounts['contracts']=$contracts;
        return $homeCounts;
    }
}
