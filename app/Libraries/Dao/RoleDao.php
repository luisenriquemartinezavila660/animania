<?php
namespace App\Libraries\Dao;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Permission;

class RoleDao{
    public function select(){
        $roles = Role::where('roles.status','=',1)
                ->select('roles.id','roles.name','roles.display_name','roles.description')
                ->get();
        return $roles;
    }
    public function insert($request){
        $idrole= Role::count();
        $role = new Role();
        $role->id=$idrole+1;
        $role->name=$request['name'];
        $role->display_name=$request['display_name'];
        $role->description=$request['description'];
        $role->save();
        $role->permissions()->attach($request['permission_id']);

    }
    public function get($id){
        $role= Role::find($id);
        return $role;

    }
    public function update($request,$id){
        $role= Role::find($id);
        $role->name=$request['name'];
        $role->display_name=$request['display_name'];
        $role->description=$request['description'];
        $role->save();
        $role->permissions()->sync($request['permission_id']);
    }
    public function delete($id){
        $delete = Role::find($id);
        $delete->status=0;
        $delete->save();
    }
}
