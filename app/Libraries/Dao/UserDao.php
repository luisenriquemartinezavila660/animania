<?php
namespace App\Libraries\Dao;
use Illuminate\Support\Facades\DB;
use App\User;

class UserDao
{
    public function select(){
        $users = User::where('users.status','=',1)
                ->select('users.id','users.name','users.age','users.telephone','users.email','users.password','users.role_id',)
                ->get();
        return $users;
    }
    public function insert($request){
        $user= new User();
        $user->name=$request['name'];
        $user->age=$request['age'];
        $user->telephone=$request['telephone'];
        $user->email=$request['email'];
        $user->password=\Hash::make($request['password']);
        //$user->roles()->attach($request['role_id']);
        $user->role_id=$request['role_id'];
        $user->save();
    }
    public function get($id){
        $user = User::find($id);
        return $user;
    }
    public function update($request,$id){
        User::find($id)->update($request);
    }
    public function delete($id){
        $delete = User::find($id);
        $delete->status=0;
        $delete->save();

    }
}
