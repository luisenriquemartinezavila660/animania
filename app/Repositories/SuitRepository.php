<?php

namespace App\Repositories;

use App\Models\Suit;
use App\Repositories\BaseRepository;

/**
 * Class SuitRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:49 am UTC
*/

class SuitRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'character',
        'garment_id',
        'stock',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Suit::class;
    }
}
