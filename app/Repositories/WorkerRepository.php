<?php

namespace App\Repositories;

use App\Models\Worker;
use App\Repositories\BaseRepository;

/**
 * Class WorkerRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:46 am UTC
*/

class WorkerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'age',
        'address',
        'telephone',
        'email',
        'post_id',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Worker::class;
    }
}
