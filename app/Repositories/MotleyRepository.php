<?php

namespace App\Repositories;

use App\Models\Motley;
use App\Repositories\BaseRepository;

/**
 * Class MotleyRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:52 am UTC
*/

class MotleyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'character',
        'source',
        'stock',
        'status',
        'cost'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Motley::class;
    }
}
