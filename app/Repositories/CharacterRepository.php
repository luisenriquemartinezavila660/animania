<?php

namespace App\Repositories;

use App\Models\Character;
use App\Repositories\BaseRepository;

/**
 * Class CharacterRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:57 am UTC
*/

class CharacterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'source',
        'photo',
        'status',
        'cost'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Character::class;
    }
}
