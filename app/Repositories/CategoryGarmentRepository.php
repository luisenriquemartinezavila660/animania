<?php

namespace App\Repositories;

use App\Models\CategoryGarment;
use App\Repositories\BaseRepository;

/**
 * Class CategoryGarmentRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:38 am UTC
*/

class CategoryGarmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryGarment::class;
    }
}
