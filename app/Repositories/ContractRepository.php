<?php

namespace App\Repositories;

use App\Models\Contract;
use App\Repositories\BaseRepository;

/**
 * Class ContractRepository
 * @package App\Repositories
 * @version April 17, 2020, 10:57 pm UTC
*/

class ContractRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'package_id',
        'name',
        'address',
        'name_celebrate',
        'date_celebrated',
        'date_contract',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contract::class;
    }
}
