<?php

namespace App\Repositories;

use App\Models\Garment;
use App\Repositories\BaseRepository;

/**
 * Class GarmentRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:41 am UTC
*/

class GarmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'type_garment',
        'stock'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Garment::class;
    }
}
