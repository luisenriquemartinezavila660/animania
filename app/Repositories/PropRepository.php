<?php

namespace App\Repositories;

use App\Models\Prop;
use App\Repositories\BaseRepository;

/**
 * Class PropRepository
 * @package App\Repositories
 * @version March 28, 2020, 5:41 am UTC
*/

class PropRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'category_prop_id',
        'stock'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Prop::class;
    }
}
