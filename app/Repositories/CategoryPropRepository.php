<?php

namespace App\Repositories;

use App\Models\CategoryProp;
use App\Repositories\BaseRepository;

/**
 * Class CategoryPropRepository
 * @package App\Repositories
 * @version March 28, 2020, 7:36 am UTC
*/

class CategoryPropRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryProp::class;
    }
}
