<?php

namespace App\Repositories;

use App\Models\CategoryCharacter;
use App\Repositories\BaseRepository;

/**
 * Class CategoryCharacterRepository
 * @package App\Repositories
 * @version March 28, 2020, 5:48 am UTC
*/

class CategoryCharacterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryCharacter::class;
    }
}
