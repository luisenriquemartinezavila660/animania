<?php

namespace App\Repositories;

use App\Models\Animator;
use App\Repositories\BaseRepository;

/**
 * Class AnimatorRepository
 * @package App\Repositories
 * @version March 28, 2020, 5:47 am UTC
*/

class AnimatorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'photo',
        'experience',
        'telephone',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Animator::class;
    }
}
